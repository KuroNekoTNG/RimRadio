# RimRadio
RimRadio is an open-source modification to the game RimWorld.
It enables users to select which music pack they want to listen to and to skip around songs.

## Why use this mod?
Stated above, this mod allows users to change which music pack they are listening to.
Granted that the pack adds at least 1 Station_*.xml file.
It also allows the user to change which song they are listening to.

## What can I do in this mod?
This mod allows you to play any song at any time. This means that you do not have to wait for the game to start playing music.
It also allows you to skip to the next song, go back to a previous song, and pause the music.
If the player is too big, there is also a mini version that will display the information of the song on 1 line along with the station icon.
Speaking of, the station icon is also a button, on the mini player, it will take you back to the full player.
On the big player, it will open a selection of stations to choose from.

## I am a creator of a music pack, how do I make these Station_*.xml file(s)?
It is simple, you can create it using any text editor you want, I'd recommend Visual Studio Code.
Just follow [this](https://gitlab.com/KuroNekoTNG/RimRadio/snippets/1803933) snippet when creating it.

## What happens if I don't add it?
RimRadio will throw all songs it does not know into an Unknown Station from [MOD_NAME].
This means that your music pack will still have a station, but it will be a bland one.