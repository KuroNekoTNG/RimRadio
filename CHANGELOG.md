[v2.2.7](https://gitlab.com/KuroNekoTNG/RimRadio/tags/v2.2.7):
* Recompiled to support RimWorld 1.2.
* Fixed issue on startup of there being an update.
    * Changed to where assemblies are located in the Current folder instead of the root.
        * This is because each time RimWorld updates, a new folder for that version will be created holding the before assemblies.
* Fixed linking to old workshop URL.
    * RimWorld renamed it making me think it just created a new one.
    
 [v2.2.6](https://gitlab.com/KuroNekoTNG/RimRadio/tags/v2.2.6):
 * Added ability to disable certain songs in the All Station inside settings.
 * Refactored some code so less garbage should be produced.
 * Fixed logic issue where it would keep the picked song as null when there was only 1 song to choose from.
 * Added filtering to the loaded stations to ensure none were empty or had null clips.
     
 [v2.2.4](https://gitlab.com/KuroNekoTNG/RimRadio/tags/v2.2.4):
 * Fixed issue with .csproj not copying the LoadFolders.xml.
      
 [v2.2.3](https://gitlab.com/KuroNekoTNG/RimRadio/tags/v2.2.3):
 * Fixed `NullReferenceException` when creating Runtime Stations.
    * Don't know what causes it in the previous build, but in this one it was because I failed to request the download.
* Upgraded from .NET 3.5 to .NET 4.7.2. (Thanks Ludeon for using Unity 2019.2, really hated .NET 3.5.)
* Changed `WWW` to `UnityWebRequest`.
* Changed from `Texture2D.LoadImage` to `ImageConversion.LoadImage`.
* Made the code much more readable.
* Made frequently used lines into new class.
* Used build 2.2.1 (commit: [d04c3eba](https://gitlab.com/KuroNekoTNG/RimRadio/-/commit/d04c3eba7c476b7669a094388f3cf754df5c56e9)) for 1.0 folder.
    * Yay backwards compatibility!

[v2.2.1](https://gitlab.com/KuroNekoTNG/RimRadio/tags/v2.2.1):
* Fixed bug where Unity destroyed the AudioSource needed for the radio.
* Replaced logic that somehow got removed.

[v2.2.0](https://gitlab.com/KuroNekoTNG/RimRadio/tags/v2.2.0):
* Improved audio loading.
	* Audio is now streamed instead of loaded into memory.
	* Also means that the cache is no longer needed for faster loading.
	* NAudio support has been removed due to conflicts non-Windows platforms.
* Improved UI Experience.
	* Now users are informed when the song changes.
	* Mini player is now a secondary option.
		* By default, the player will hide instead of going to a minimum state.
		* Mini player and hidden are switchable. If you prefer the Mini player, you can change this within settings.
	* Fixed weird horizontal scrollbar issue.
		* Had to brute force this. - Kyu Vulpes

[v2.1.1](https://gitlab.com/KuroNekoTNG/RimRadio/-/tags/v2.1.1):
* Added Runtime Station support.
* Added mp3 support.
* Made it search for wav files in selected directories.
* Made it search for ogg files in selected directories.
* Made it search for mp3 files in selected directories.
* The groundwork for better Runtime Stations laid.

[v2.0.9](https://gitlab.com/KuroNekoTNG/RimRadio/tags/v2.0.9):
* Mini player fully implemented.
	* Mini player is able to go back to the full player.
	* Mini player cycles through the info of a song.
	* Mini player will update when the song changes.
* Settings were added.
	* Settings Window implemented.
	* Allows ignoring of time of day.
	* Allows ignoring of season.
	* Allowed ability to switch to the classic media player.
* Core rework completed.

[v2.0.3-beta](https://gitlab.com/KuroNekoTNG/RimRadio/tags/v2.0.3-beta):
* [ ]  Mini player fully implemented.
	* [x]  Mini player is able to go back to the full player.
	* [x]  Mini player cycles through the info of a song.
	* [ ]  Mini player will update when the song changes.
* [ ]  Settings were added.
	* [x]  Settings Window implemented.
	* [x]  Allows ignoring of time of day.
	* [x]  Allows ignoring of season.
	* [ ]  Allows users to import own song.
* [x]  Core rework completed.

[v2.0.1-beta](https://gitlab.com/KuroNekoTNG/RimRadio/tags/v2.0.1-beta):
* [x]  MiniPlayer implemented.
* [ ]  MiniPlayer shows information about the song.
	* [x]  Goes through all the information.
	* [ ]  Does it on a player-set limit.
* [x]  MiniPlayer shows station icon.

[v2.0.0-beta](https://gitlab.com/KuroNekoTNG/RimRadio/tags/v2.0.0-beta2)
* [X]  Reworked parts of the core.
	* [X]  Reworked how station loading worked.
* [X]  Removed support for the need of creating DLLs.
	* This was honestly stupid, but I was new to modding RimWorld.
	* [ ]  Use XML to load stations.
		* [X]  Find a place to store XML.
		* [ ]  Make loading of XML from those locations.

[v1.1.0](https://gitlab.com/KuroNekoTNG/RimRadio/tags/V1.1.0)
- Updated to be compatible with B19.
- Fixed some issues with Visual Studio not properly compiling or finding executable.
- Added features for future support.
- Added the ability to drag the Player around.

[v1.0.0](https://gitlab.com/KuroNekoTNG/RimRadio/tags/V1.0.0)
Initial