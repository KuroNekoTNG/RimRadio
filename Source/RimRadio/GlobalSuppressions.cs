﻿// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage(
	"Code Quality",
	"IDE0051:Remove unused private members",
	Justification = "<Pending>",
	Scope = "member",
	Target = "~M:RimRadio.Patching.UIRoot_EntryPatching.StartRuntimeStationLoading"
)]
[assembly: SuppressMessage(
	"Performance",
	"CA1810:Initialize reference type static fields inline",
	Justification = "<Pending>",
	Scope = "member",
	Target = "~M:RimRadio.RimRadioSettings.#cctor"
)]
[assembly: SuppressMessage(
	"Performance",
	"CA1810:Initialize reference type static fields inline",
	Justification = "<Pending>",
	Scope = "member",
	Target = "~M:RimRadio.Patching.UIRoot_EntryPatching.#cctor"
)]
[assembly: SuppressMessage(
	"Performance",
	"CA1810:Initialize reference type static fields inline",
	Justification = "<Pending>",
	Scope = "member",
	Target = "~M:RimRadio.Patching.HarmonyPatches.#cctor"
)]
[assembly: SuppressMessage(
	"Performance",
	"CA1810:Initialize reference type static fields inline",
	Justification = "<Pending>",
	Scope = "member",
	Target = "~M:RimRadio.Runtime.RuntimeStationCreator.#cctor"
)]
[assembly: SuppressMessage(
	"Performance",
	"CA1810:Initialize reference type static fields inline",
	Justification = "<Pending>",
	Scope = "member",
	Target = "~M:RimRadio.Runtime.Stations.RuntimeStation.#cctor"
)]
[assembly: SuppressMessage(
	"Performance",
	"CA1822:Mark members as static",
	Justification = "<Pending>",
	Scope = "member",
	Target = "~M:RimRadio.Music.RadioPlayer.IsNight~System.Boolean"
)]
[assembly: SuppressMessage(
	"Performance",
	"CA1819:Properties should not return arrays",
	Justification = "<Pending>",
	Scope = "member",
	Target = "~P:RimRadio.Music.Info.CrossRefStation.CrossRefSongs"
)]
[assembly: SuppressMessage(
	"Performance",
	"CA1822:Mark members as static",
	Justification = "<Pending>",
	Scope = "member",
	Target = "~M:RimRadio.Music.RadioPlayer.ValidPlayingSeason(RimRadio.Music.SongInfo)~System.Boolean"
)]
[assembly: SuppressMessage(
	"Code Quality",
	"IDE0051:Remove unused private members",
	Justification = "Needed for Harmony Patching.",
	Scope = "member",
	Target = "~M:RimRadio.Patching.MusicManagerPlayPatching.Prefix(RimWorld.MusicManagerPlay)~System.Boolean"
)]
[assembly: SuppressMessage(
	"Performance",
	"CA1822:Mark members as static",
	Justification = "Doesn't matter, it essentially acts as static since only one is created.",
	Scope = "member",
	Target = "~M:RimRadio.RimRadioMod.AddSongToUnknown(Verse.SongDef,System.Collections.Generic.Dictionary{System.String,RimRadio.Music.RadioStation}@)"
)]
[assembly: SuppressMessage(
	"Performance",
	"CA1822:Mark members as static",
	Justification = "<Pending>",
	Scope = "member",
	Target = "~M:RimRadio.RimRadioMod.GetStationIcon(System.String)~UnityEngine.Texture2D"
)]
[assembly: SuppressMessage(
	"Design",
	"CA1031:Do not catch general exception types",
	Justification = "Sometimes you need to since you don't know what exception is going to happen."
)]
[assembly: SuppressMessage(
	"Performance",
	"CA1822:Mark members as static",
	Justification = "Feels better making it part of the object and not the class. Espeically knowing that only 1 should exist.",
	Scope = "member",
	Target = "~P:RimRadio.Music.RadioPlaylistManager.AllSongStation.AllCount"
)]