﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using HarmonyLib;
using RimRadio.Runtime;
using UnityEngine;
using Verse;

namespace RimRadio.Patching {
	[HarmonyPatch( typeof( UIRoot_Entry ) ), HarmonyPatch( "Init" )]
	// ReSharper disable once UnusedType.Global
	internal static class UIRoot_EntryPatching {
		#region Language strings

		#region Language Keys

		private const string NEW_UPDATE_KEY       = "NewUpdate";
		private const string NEW_UPDATE_TITLE_KEY = "NewUpdateTitle";
		private const string BUTTON_CONTINUE_KEY  = "ContinueButton";

		private const string LOADING_RUNTIME_STATION_KEY = "LoadingRuntimeStations";

		#endregion

		#region Language Translations

		private static readonly string NEW_UPDATE;
		private static readonly string NEW_UPDATE_TITLE;
		private static readonly string BUTTON_CONTINUE;

		#endregion

		#endregion

		private static readonly string shaFileLocation;
		private static readonly string asmFileLocation;

		private static bool alreadyInit;

		static UIRoot_EntryPatching() {
			shaFileLocation = Path.Combine( Application.persistentDataPath, "RimRadio.sha" );
			// Remember to change this to the correct version detector.
			asmFileLocation = Path.Combine( RimRadioMod.RimRadioInstence.Content.RootDir, "Current", "Assemblies" );

			//This region holds a bunch of calls to get localizations.

			#region Translation Fetching

			GetTranslate( NEW_UPDATE_KEY, out NEW_UPDATE );
			GetTranslate( NEW_UPDATE_TITLE_KEY, out NEW_UPDATE_TITLE );
			GetTranslate( BUTTON_CONTINUE_KEY, out BUTTON_CONTINUE );

			#endregion
		}

		// Meant to kick off the loading of RuntimeStations.
		// It does this by having Harmony patch itself after the UIRoot_Entry.Init function.
		[HarmonyPostfix]
		[SuppressMessage( "ReSharper", "UnusedMember.Local" )]
		private static void StartRuntimeStationLoading() {
			/* `UIRoot_Entry` runs every time the game starts.
			 * To stop from running this multiple time, a flag is set.
			 * Once this runs once, the flag is set and this will stop it from running again.
			 */
			if ( alreadyInit ) {
				return;
			}

			Logging.Message( "Hello Log Viewer, Kyu Vulpes here, prepare for some log spamming!" );

			alreadyInit = true;

			if ( Updated() ) {
				Find.WindowStack.Add(
					new Dialog_MessageBox(
						NEW_UPDATE,             // The translated string of the dialog.
						BUTTON_CONTINUE,        // What the first button says.
						InformedAboutUpdate,    // Method to call.
						title: NEW_UPDATE_TITLE // The title of the dialog box.
					)
				);
			} else {
				// Proceeds to start loading Runtime Stations.
				CallRuntimeStationLoading();
			}
		}

		/* Informs the user that there was an update.
		 * This method also updates the Hash file as well. */
		private static void InformedAboutUpdate() {
			var shas = new Dictionary<string, byte[]>();

			using ( var sha256 = SHA256.Create() ) {
				// Loops through every assembly file.
				foreach ( var asmFile in new DirectoryInfo( asmFileLocation ).EnumerateFiles( "*.dll", SearchOption.TopDirectoryOnly ) ) {
					using ( var asmStream = new FileStream( asmFile.FullName, FileMode.Open, FileAccess.Read, FileShare.Read ) ) {
						shas.Add( asmFile.Name, sha256.ComputeHash( asmStream ) );
					}
				}
			}

			using ( var shaFile = new FileStream( shaFileLocation, FileMode.Create, FileAccess.Write, FileShare.Read ) ) {
				using ( var writer = new BinaryWriter( shaFile, Encoding.Unicode ) ) {
					writer.Write( (byte) shas.Count );

					foreach ( var pair in shas ) {
						writer.Write( pair.Key );
						writer.Write( pair.Value );
					}
				}
			}

			CallRuntimeStationLoading();
		}

		/* I have no idea what this method is doing, but clearly doing something important.
		 *
		 * Never mind, figured out that I did this to show that it is still doing something and doesn't lock up RimWorld. */
		private static void CallRuntimeStationLoading() => LongEventHandler.QueueLongEvent(
			RuntimeStationCreator.LoadRuntimeStations, // This method loads runtime stations.
			LOADING_RUNTIME_STATION_KEY,               // The translation string showing what the mod is doing.
			false,                                     // Informs RimWorld not to run this asynchronously.
			( e ) => Logging.Error( $"Type: {e.GetType().FullName}\nMessage: {e.Message}\nStacktrace:\n{e.StackTrace}" )
		);

		// This method checks to see if the mod had updated or not.
		private static bool Updated() {
			// If the file does not exist, might as well show the updated message.
			if ( !File.Exists( shaFileLocation ) ) {
				return true;
			}

			// Dictionary is used because it allows for both associating the assembly name with it's hash.
			var asmSha = new Dictionary<string, byte[]>();

			using ( var sha256 = SHA256.Create() ) {
				foreach ( var asmFile in new DirectoryInfo( asmFileLocation ).EnumerateFiles( "*.dll", SearchOption.TopDirectoryOnly ) ) {
					using ( var asmStream = new FileStream( asmFile.FullName, FileMode.Open, FileAccess.Read, FileShare.Read ) ) {
						asmSha.Add( asmFile.Name, sha256.ComputeHash( asmStream ) );
					}
				}
			}

			// SHA file holds the checksum of the main assembly
			using ( var shaStream = new FileStream( shaFileLocation, FileMode.OpenOrCreate, FileAccess.Read, FileShare.Read ) ) {
				// This check is meant to see if it is the old hash file. 
				if ( shaStream.Length == 32 ) {
					// Since I am changing the hash file structure, and it is the old version, say that an update occured.
					return true;
				}

				using ( var reader = new BinaryReader( shaStream, Encoding.Unicode ) ) {
					var count = reader.ReadByte(); // Count of the number of hashes in the file.

					// Goes through each and every assembly file and calculates the hash of them.
					for ( var i = 0; i < count; ++i ) {
						var asmName     = reader.ReadString();    // Assembly file name.
						var sha         = reader.ReadBytes( 32 ); // The bytes from the Hash file.
						var computeData = asmSha[asmName];        // The computed hash.

						// Just checks to see if the hashes match.
						for ( var j = 0; j < 32; ++j ) {
							if ( sha[j] != computeData[j] ) {
								return true;
							}
						}
					}
				}
			}

			return false;
		}

		// This method exists because of the redundancy that would be doing this for every single text that would need to be translated.
		private static void GetTranslate( string key, out string translate ) {
			var didTranslate = key.TryTranslate( out var taggedTranslate );

			// I hate this update, I really do. A lot of the code broke because of the changes.
			// On a positive note, .NET 4.X instead of .NET 3.5!
			translate = taggedTranslate;

			if ( didTranslate ) {
				return;
			}

			// Reports a failed translation.
			Log.Error( $"Failed to retrieve translation for '{key}'." );
		}
	}
}