﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;
using HarmonyLib;
using UnityEngine;
using UnityEngine.Rendering;
using Verse;

namespace RimRadio.Patching {
	/// <summary>
	/// A static class who's only purpose is to inject some code and reroute some code.
	/// </summary>
	[StaticConstructorOnStartup]
	internal static class HarmonyPatches {
		// ReSharper disable once MemberCanBePrivate.Global
		internal static RimRadioMod RimRadioMod {
			get;
		}

		// ReSharper disable once PrivateFieldCanBeConvertedToLocalVariable
		private static readonly Harmony harmonyInstance;

		/// <summary>
		/// Initializes the <see cref="HarmonyPatches"/> class.
		/// </summary>
		static HarmonyPatches() {
			harmonyInstance = new Harmony( "com.kuroneko.rimradio" ); // Get's the harmony instance and stores it away.
			RimRadioMod     = LoadedModManager.GetMod<RimRadioMod>();
			
			OutputData();

			harmonyInstance.PatchAll( Assembly.GetAssembly( typeof( HarmonyPatches ) ) );

			RimRadioMod.LoadModStations();
		}

		[Conditional("DEBUG")]
		private static void OutputData() => Logging.DebugMessage(
			$"Information about runtime:\n" +
			$"\tOS: {RuntimeInformation.OSDescription}\n" +
			$"\tOS Arch: {Enum.GetName( typeof( Architecture ), RuntimeInformation.OSArchitecture )}\n" +
			$"\tFramework: {RuntimeInformation.FrameworkDescription}\n" +
			$"\tGraphics API: {Enum.GetName( typeof( GraphicsDeviceType ), SystemInfo.graphicsDeviceType )}\n" +
			$"\tUnity Version: {Application.version}\n" +
			$"\tPlatform: {Enum.GetName( typeof(RuntimePlatform), Application.platform )}\n"
		);
	}
}