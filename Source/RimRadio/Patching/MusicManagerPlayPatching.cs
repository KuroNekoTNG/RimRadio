﻿using HarmonyLib;

using RimRadio.Music;
using RimRadio.Music.UserInterface;

using RimWorld;

using Verse;

namespace RimRadio.Patching {
	[HarmonyPatch( typeof( MusicManagerPlay ) ), HarmonyPatch( "MusicUpdate" )]
	public static class MusicManagerPlayPatching {

		internal static RadioPlayer radio;
		internal static MusicPlayerWindow radioDisplay;

		/// <summary>
		/// Interrupts the MusicUpdate method and injects the RimRadio into it.
		/// </summary>
		/// <param name="__instance">The instance.</param>
		/// <returns>Always returns false to stop the method and other prefix/postfixes from starting.</returns>
		[HarmonyPriority( Priority.First )]
		private static bool Prefix( MusicManagerPlay __instance ) {
			CheckAndCreate(); // Read what this method does.
			radio.Update(); // Calls the update for the radio.

			if ( !__instance.disabled ) { // Checks to see if the MusicManagerPlay is not disabled.
				__instance.disabled = true; // Disables it.
			}

			return false; // Returns false to block the original method and any other pre/postfixes.
		}

		// Checks to make sure that those objects are created.
		private static void CheckAndCreate() {
			if ( !( radioDisplay is null ) && !( radio is null ) ) { // Checks to see if the radio display is not null and 
				foreach ( var window in Find.WindowStack.Windows ) { // For each loop that will loop through all the windows in the window stack.
					if ( window == radioDisplay ) { // Checks to see if it finds radio display.
						return; // Returns to exit the loop and the method.
					}
				}

				radioDisplay = null; // Marks it null since it should be displaying.
			} else {
				radio = RadioPlayer.Radio; // Creates a new radio player.
				radioDisplay = MusicPlayerWindow.Singleton;
			}
		}
	}
}
