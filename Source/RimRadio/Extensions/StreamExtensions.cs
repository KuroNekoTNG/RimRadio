﻿using System.IO;

public static class StreamExtensions {

	public static void Write( this Stream stream, byte[] data ) => stream.Write( data, 0, data.Length );

	public static int Read( this Stream stream, byte[] data ) => stream.Read( data, 0, data.Length );
}