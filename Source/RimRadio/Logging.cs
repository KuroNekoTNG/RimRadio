﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

using Verse;

namespace RimRadio {
	[StaticConstructorOnStartup]
	internal static class Logging {

		private const string KERNEL32_DLL_NAME = "kernel32.dll";

		static Logging() {
			Show();
		}

		private static bool HasConsole => GetConsoleWindow() != IntPtr.Zero;

		[DllImport( KERNEL32_DLL_NAME, SetLastError = true )]
		[return: MarshalAs( UnmanagedType.Bool )]
		private static extern bool AllocConsole();

		[DllImport( KERNEL32_DLL_NAME, SetLastError = true )]
		private static extern bool FreeConsole();

		[DllImport( KERNEL32_DLL_NAME, SetLastError = true )]
		private static extern IntPtr GetConsoleWindow();

		[DllImport( KERNEL32_DLL_NAME, SetLastError = true )]
		private static extern int GetConsoleOutputCp();

		[Conditional( "DEBUG" )]
		internal static void DebugMessage( string message, bool ignoreLimit = false ) => Console.WriteLine( message );

		[Conditional( "DEBUG" )]
		internal static void DebugError( string message ) {
			Console.ForegroundColor = ConsoleColor.Red;

			Console.WriteLine( message );
			Console.ResetColor();
		}

		[Conditional( "DEBUG" )]
		internal static void DebugError( Exception e ) => DebugError(
			string.Format(
					"Exception:\t{0}\nMessage:\t{1}\nStacktrace:\n{2}",
					e.GetType().FullName,
					e.Message,
					e.StackTrace
				)
		);

		internal static void Message( string message, bool ignoreLimit = false ) {
			if ( HasConsole ) {
				DebugMessage( message, ignoreLimit );
			} else {
				Log.Message( message, ignoreLimit );
			}
		}

		internal static void Error( string message, bool ignoreLimit = true ) {
			if ( HasConsole ) {
				DebugError( message );
			} else {
				Log.Error( message, ignoreLimit );
			}
		}

		internal static void Error( Exception e, bool ignoreLimit = true ) => Error(
			string.Format(
				"Exception:\t{0}\nMessage:\t{1}\nStacktrace:\n{2}",
				e.GetType().FullName,
				e.Message,
				e.StackTrace
			),
			ignoreLimit
		);

		[Conditional( "DEBUG" )]
		private static void Show() {
			if ( !HasConsole ) {
				AllocConsole();
				InvalidateOutAndError();
			}
		}

		private static void InvalidateOutAndError() {
			var type = typeof( Console );

			var _out = type.GetField( "stdout", BindingFlags.Static | BindingFlags.NonPublic );
			var _err = type.GetField( "stderr", BindingFlags.Static | BindingFlags.NonPublic );
			var initOut = type.GetConstructor( BindingFlags.Static | BindingFlags.NonPublic, null, Array.Empty<Type>(), null );

			_out.SetValue( null, null );
			_err.SetValue( null, null );

			initOut.Invoke( null, Array.Empty<object>() );
		}

	}
}