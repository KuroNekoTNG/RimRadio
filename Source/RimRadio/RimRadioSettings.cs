﻿using System;
using RimRadio.Music;
using UnityEngine;

using Verse;

namespace RimRadio {

	[StaticConstructorOnStartup]
	internal class RimRadioSettings : ModSettings {

		internal const float COLOR_SAMPLE_RECT_HEIGHT = 16f;

		internal static readonly string DEFAULT_MUSIC_DIR;

		#region Playlist Manager Settings
		internal static bool ignoreAllowedSeasons; // A boolean for ignoring the allowed seasons restriction.
		internal static bool ignoreTimeOfDay; // A boolean for ignoring time of day restriction.
		internal static bool shuffle; // Rather to shuffle around the station or to go in order.
		#endregion

		#region Player Window Settings
		internal static bool useClassic; // Rather the user wants to uses the classic player or the new player.

		internal static bool miniHides;

		internal static float waitTime; // How long until the information changes.

		internal static string musicDirs; // A semicolon-seperated value for where to look for music files.

		internal static Vector2 winPos;
		internal static Vector2 winSize;

		internal static Vector2 DefaultWindowPosition => defaultWinPos;

		internal static Vector2 DefaultWindowSize {
			get;
		}

		internal static Color PlayerColor {
			get {
				var pColor = Color.HSVToRGB( playerHue, playerSaturation, playerValue ); // Creates a new color from the HSV passed to it.

				pColor.a = playerAlpha; // Set's the alpha.

				return pColor; // Returns the color.
			}
		}

		internal static Color PlayerScrollColor {
			get {
				var playerColor = PlayerColor;

				playerColor.r -= playerColor.r * 0.75f; // Creates a darker value of red.
				playerColor.g -= playerColor.g * 0.75f; // Creates a darker value of green.
				playerColor.b -= playerColor.b * 0.75f; // Creates a darker value of blue.

				return playerColor; // Returns the color.
			}
		}

		internal static Color HighlightColor {
			get {
				var highlightColor = Color.HSVToRGB( highlightHue, highlightSaturation, highlightValue ); // Grabs a color from the HSV.

				highlightColor.a = highlightAlpha; // Set's alpha to the alpha value.

				return highlightColor; // Returns the color.
			}
		}

		private static float playerHue, playerSaturation, playerValue, playerAlpha;
		private static float highlightHue, highlightSaturation, highlightValue, highlightAlpha;

		private static string waitTimeBuffer; // The buffer for the wait time field.

		private static Vector2 mainPos;
		private static Vector2 defaultWinPos;
		private static Vector2 songPos;

		private static Rect mainViewRect;
		private static Rect songViewRect;
		#endregion

		static RimRadioSettings() {
			var res = Screen.currentResolution;

			DefaultWindowSize = new Vector2( 512f, 256f );
			defaultWinPos = new Vector2( res.width - DefaultWindowSize.x, 0f );

			winPos = defaultWinPos;
			winSize = DefaultWindowSize;

			DEFAULT_MUSIC_DIR = string.Format( "{0};", Environment.GetFolderPath( Environment.SpecialFolder.MyMusic ) );

			musicDirs = DEFAULT_MUSIC_DIR;
		}

		public RimRadioSettings() : base() {
		}

		public override void ExposeData() {
			// Playlist Manager Settings
			Scribe_Values.Look( ref ignoreTimeOfDay, "IgnoreTimeOfDay", false, true );
			Scribe_Values.Look( ref ignoreAllowedSeasons, "IgnoreAllowedSeasons", false, true );
			Scribe_Values.Look( ref shuffle, "Shuffle", false, true );
			// Player Window Settings
			Scribe_Values.Look( ref useClassic, "UseClassic", false, true );
			Scribe_Values.Look( ref miniHides, "UseNewMini", true, true );
			Scribe_Values.Look( ref waitTime, "WaitTime", 15f, true );
			// Player Background Color property.
			Scribe_Values.Look( ref playerHue, "PlayerHue", 0.5833333f, true );
			Scribe_Values.Look( ref playerSaturation, "PlayerSaturation", 0.275862f, true );
			Scribe_Values.Look( ref playerValue, "PlayerValue", 0.1137255f, true );
			Scribe_Values.Look( ref playerAlpha, "PlayerAlpha", 1f, true );
			// Highlight Color property.
			Scribe_Values.Look( ref highlightHue, "HighlightHue", 0.5833333f, true );
			Scribe_Values.Look( ref highlightSaturation, "HighlightSaturation", 0.275862f, true );
			Scribe_Values.Look( ref highlightValue, "HighlightValue", 0.1137255f, true );
			Scribe_Values.Look( ref highlightAlpha, "HighlightAlpha", 1f, true );
			// Window properties.
			Scribe_Values.Look( ref winPos, "WindowPosition", defaultWinPos, true );
			// Converter Settings.
			try {
				Scribe_Values.Look( ref musicDirs, "MusicDirs", DEFAULT_MUSIC_DIR, true );
			} catch {
				Log.Warning( "I have no idea what is going here, and quite frankly, fuck it." );
			}
		}

		internal static void UpdateDefaultPos() {
			var res = Screen.currentResolution;
			defaultWinPos = new Vector2( res.width - DefaultWindowSize.x, 0f );
		}

		internal static void DoSettingsWindow( Rect inRect ) {
			var listing = new Listing_Standard();
			var selectionRect = new Rect( inRect );

			selectionRect.yMax -= 40f;

			if ( string.IsNullOrEmpty( waitTimeBuffer ) ) {
				waitTimeBuffer = waitTime.ToString( "N2" );
			}

			// Starts the listing of settings.
			listing.Begin( inRect );

			listing.BeginScrollView( selectionRect, ref mainPos, ref mainViewRect );

			GlobalSettingsListing( ref listing, out var reset );

			listing.Gap(); // Used to seperate the sections.

			MiniDisplaySettingsListing( ref listing );

			//selectionRect.yMax = viewRect.yMax;

			listing.Gap();

			CreateSongChecklist( ref listing, ref inRect );

			listing.Gap();

			listing.EndScrollView( ref mainViewRect );

			// Ends the listing of settings.
			listing.End();

			if ( reset ) {
				ResetSetting();

				Find.WindowStack.Add( new Dialog_MessageBox(
					"RestartQuestion".Translate(),
					"RestartOk".Translate(),
					GenCommandLine.Restart,
					"RestartNo".Translate(),
					title: "RestartTitle".Translate()
				) );
			}
		}

		private static void ResetSetting() {
			// Music Manager reset.
			ignoreTimeOfDay = default;
			ignoreAllowedSeasons = default;
			shuffle = default;
			// Player reset.
			useClassic = default;
			waitTime = 15f;
			highlightHue = playerHue = 0.5833333f;
			highlightSaturation = playerSaturation = 0.275862f;
			highlightValue = playerValue = 0.1137255f;
			highlightAlpha = playerAlpha = 1f;
			winPos = defaultWinPos;
			// MP3 support/Converter settings.
			musicDirs = DEFAULT_MUSIC_DIR;

			RimRadioMod.RimRadioInstence.WriteSettings();
		}

		private static void MiniDisplaySettingsListing( ref Listing_Standard listing ) {
			CreateSection( ref listing, "MiniDisplaySettingsLabel".Translate() );

			listing.TextFieldNumericLabeled( "MiniDisplayWaitLabel".Translate(), ref waitTime, ref waitTimeBuffer, 0.5f );
		}

		private static void GlobalSettingsListing( ref Listing_Standard listing, out bool reset ) {

			CreateSection( ref listing, "GlobalSettingsLabel".Translate() );

			reset = listing.ButtonTextLabeled( "ResetLabel".Translate(), "ResetButtonLabel".Translate() );

			// Creates a checkbox with a lable for ingore time of day toggle.
			listing.CheckboxLabeled( "IgnoreToD".Translate(), ref ignoreTimeOfDay, "IgnoreToDDesc".Translate() );
			// Creates a checkbox with a label for ignore allowed seasons toggle.
			listing.CheckboxLabeled( "IgnoreAS".Translate(), ref ignoreAllowedSeasons, "IgnoreASDesc".Translate() );
			// Creates a checkbox with a label for using the classic player toggle.
			listing.CheckboxLabeled( "UseClassic".Translate(), ref useClassic, "UseClassicDesc".Translate() );
			// Creates a checkbox with a label for using shuffle.
			listing.CheckboxLabeled( "Shuffle".Translate(), ref shuffle, "ShuffleDesc".Translate() );
			// Creates a checkbox with a label for changing the behaviour of the mini button.
			listing.CheckboxLabeled( "HideInsteadMini".Translate(), ref miniHides, "HideInsteadMiniDesc".Translate() );

			musicDirs = listing.TextEntry( musicDirs );

			CreatePlayerBackgroundSection( ref listing );
			CreateHighlightColorSection( ref listing );
		}

		private static void CreateHighlightColorSection( ref Listing_Standard listing ) {
			Rect colorSampleRect;

			CreateSection( ref listing, "HighlightSectionLabel".Translate() );

			// Creates a label telling what the slider below it does.
			listing.Label( "HighlightHueSliderLabel".Translate(), tooltip: "HighlightHueSliderDesc".Translate() );
			// Grabs a new float from the slider and assigns it to hue.
			highlightHue = listing.Slider( highlightHue, 0f, 1f );
			// Creates a label telling what the slider below it does.
			listing.Label( "HighlightSaturationSliderLabel".Translate(), tooltip: "HighlightSaturationSliderDesc".Translate() );
			// Grabs a new float from the slider and assigns it to saturation.
			highlightSaturation = listing.Slider( highlightSaturation, 0f, 1f );
			// Creates a label telling what the slider below it does.
			listing.Label( "HighlightValueSliderLabel".Translate(), tooltip: "HighlightValueSliderDesc".Translate() );
			// Grabs a new float from the slider and assigns it to value.
			highlightValue = listing.Slider( highlightValue, 0f, 1f );
			// Creates a label telling what the slider below it does.
			listing.Label( "HighlightAlphaSliderLabel".Translate(), tooltip: "HighlightAlphaSliderDesc".Translate() );
			// Grabs a new float from the slider and assigns it to alpha.
			highlightAlpha = listing.Slider( highlightAlpha, 0f, 1f );
			// Creates a label telling what the rect below is for.
			listing.Label( "PreviewLabel".Translate() );
			// Grabs a rect that will be used to show a preview color.
			colorSampleRect = listing.GetRect( COLOR_SAMPLE_RECT_HEIGHT );
			// Creates a rect with the color created.
			Widgets.DrawRectFast( colorSampleRect, HighlightColor );
		}

		private static void CreatePlayerBackgroundSection( ref Listing_Standard listing ) {
			Rect colorSampleRect;

			CreateSection( ref listing, "PlayerBackgroundColorLabel".Translate() );

			// Creates a label telling what the slider below it does.
			listing.Label( "PlayerHueSliderLabel".Translate(), tooltip: "PlayerHueSliderDesc".Translate() );
			// Grabs a new float from the slider and assigns it to hue.
			playerHue = listing.Slider( playerHue, 0f, 1f );
			// Creates a label telling what the slider below it does.
			listing.Label( "PlayerSaturationSliderLabel".Translate(), tooltip: "PlayerSaturationSliderDesc".Translate() );
			// Grabs a new float from the slider and assigns it to saturation.
			playerSaturation = listing.Slider( playerSaturation, 0f, 1f );
			// Creates a label telling what the slider below it does.
			listing.Label( "PlayerValueSliderLabel".Translate(), tooltip: "PlayerValueSliderDesc".Translate() );
			// Grabs a new float from the slider and assigns it to value.
			playerValue = listing.Slider( playerValue, 0f, 1f );
			// Creates a label telling what the slider below it does.
			listing.Label( "PlayerAlphaSliderLabel".Translate(), tooltip: "PlayerAlphaSliderDesc".Translate() );
			// Grabs a new float from the slider and assigns it to alpha.
			playerAlpha = listing.Slider( playerAlpha, 0f, 1f );
			// Creates a label telling what the rect below is for.
			listing.Label( "PreviewLabel".Translate() );
			// Grabs a rect that will be used to show a preview color.
			colorSampleRect = listing.GetRect( COLOR_SAMPLE_RECT_HEIGHT );
			// Creates a rect with the color created.
			Widgets.DrawRectFast( colorSampleRect, PlayerColor );
		}

		private static void CreateSongChecklist( ref Listing_Standard listing, ref Rect inRect ) {
			Logging.DebugMessage( "Start of CreateSongChecklist." );

			// Grabs and calculates things needed for creating the view.
			var allStation = ( RadioPlaylistManager.AllSongStation )RadioPlaylistManager.Singleton[0];
			var height = ( Text.LineHeight + listing.verticalSpacing ) * allStation.Songs.Count;
			var viewHeight = ( Text.LineHeight + listing.verticalSpacing ) * ( allStation.Songs.Count > 12 ? 12 : allStation.Songs.Count );

			// Makes it create the section name.
			CreateSection( ref listing, "Runtime Song Selection" );

			Logging.DebugMessage( $"Just named section. Attempting to make `{nameof( listing )}` create Rect." );

			#pragma warning disable 612
			var newListing = listing.BeginSection( viewHeight );
			#pragma warning restore 612

			// Creates a rect from the new Listing with the calculated height.
			songViewRect = newListing.GetRect( height );

			// Something weird happens here. If the newListing height isn't the same as the GetRect height,
			// then x will be the width, which isn't good. So this set's it back to 0.
			songViewRect.x = 0f;

			var viewRect = new Rect( 0f, 0f, songViewRect.width, viewHeight );

			songViewRect.width -= 16f;

			Logging.DebugMessage( $"Beginning Scrollview.\nSong Count:\t{allStation.Songs.Count}" );

			newListing.BeginScrollView( viewRect, ref songPos, ref songViewRect );

			// Goes through each and every single song in the station and displays it.
			for ( int i = 0; i < allStation.Songs.Count; ++i ) {
				var enabled = allStation.IsSongEnabled( i );
				var enabledCopy = enabled;

				newListing.CheckboxLabeled( $"[{i + 1}] {allStation.Songs[i].ToString()}", ref enabled );

				if ( enabled != enabledCopy ) {
					if ( enabled ) {
						allStation.EnableSong( i );
					} else {
						allStation.DisableSong( i );
					}
				}
			}

			Logging.DebugMessage( "Ending Scrollview." );

			newListing.EndScrollView( ref songViewRect );

			listing.EndSection( newListing );
		}

		private static void CreateSection( ref Listing_Standard listing, string sectionTitle = "" ) {
			// Creates a label.
			listing.Label( sectionTitle );

			// Creates a gap with a line with the default height.
			listing.GapLine();
		}
	}
}