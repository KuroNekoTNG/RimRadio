﻿using System;
using System.Collections.Generic;

using RimWorld;

using UnityEngine;

using Verse;

using UnityRandom = UnityEngine.Random;

namespace RimRadio.Music {

	/// <summary>
	/// This class represents the radio that will play the music.
	/// </summary>
	public class RadioPlayer {

		public static RadioPlayer Radio {
			get;
		} = new RadioPlayer();

		/// <summary>
		/// This is the point at which day starts.
		/// </summary>
		public const double DAY_BREAK = 0.200000002980232;

		/// <summary>
		/// This is the point as which night time begins.
		/// </summary>
		public const double NIGHT_FALL = 0.699999988079071;

		/// <summary>
		/// Used to inform that the station has changed.
		/// </summary>
		/// <param name="newValue">The new station.</param>
		public delegate void ChangeNotifier( int newValue );

		/// <summary>
		/// Holds the delegate that was created.
		/// </summary>
		public ChangeNotifier StationChangedNotifier;

		public ChangeNotifier SongChangedNotifier;

		/// <summary>
		/// The int that represents the current station.
		/// </summary>
		public int Station {
			get => station;

			set {
				previousStation = station;
				station = value;

				StationChangedNotifier( value );
			}
		}

		/// <summary>
		/// Get's the current song's index that
		/// </summary>
		public int CurrentSong {
			get => song;

			set => SelectSong( value );
		}

		/// <summary>
		/// Checks to see if the song has changed.
		/// </summary>
		public bool SongChanged => song != previousSong;

		/// <summary>
		/// Gives the length of the song in mm:ss.
		/// </summary>
		public string LengthOfSong {
			get {
				var length = radioSource.clip.length;
				var minutes = ( int )( length / 60 );
				var seconds = ( int )( length % 60 );

				return $"{minutes}:{seconds.ToString( "D2" )}";
			}
		}

		/// <summary>
		/// Gives the current timestamp in mm:ss
		/// </summary>
		public string CurrentTime {
			get {
				var length = radioSource.time;
				var minutes = ( int )( length / 60 );
				var seconds = ( int )( length % 60 );

				return $"{minutes}:{seconds.ToString( "D2" )}";
			}
		}

		/// <summary>
		/// Get's the current song's title.
		/// </summary>
		public string CurrentSongTitle => playlistManager[Station].Songs[CurrentSong].Title;

		/// <summary>
		/// Get's the current song's artist.
		/// </summary>
		public string CurrentSongArtist => playlistManager[Station].Songs[CurrentSong].Artist;

		/// <summary>
		/// Get's the current song's album.
		/// </summary>
		public string CurrentSongAlbum => playlistManager[Station].Songs[CurrentSong].Album;

		/// <summary>
		/// A public boolean that represents if the station has been changed or not.
		/// </summary>
		public bool StationChanged => Station != previousStation;

		public bool RadioSourceAlive => radioSource is AudioSource && radioSource && radioSource.gameObject is GameObject && radioSource.gameObject;

		/// <summary>
		/// Tells the radio when to pause.
		/// </summary>
		public bool Pause {
			get => pause;

			set {
				pause = value;

				if ( value && radioSource.isPlaying ) {
					radioSource.Pause();
				} else if ( !value && !radioSource.isPlaying ) {
					radioSource.Play();
				}
			}
		}

		/// <summary>
		/// Returns the current time of the song in float.
		/// See also: <seealso cref="AudioSource.time"/>
		/// </summary>
		public float FloatTime => radioSource.time;

		/// <summary>
		/// Returns the length of the clip as a float.
		/// See also: <seealso cref="AudioClip.length"/>
		/// </summary>
		public float FloatLengthTime => radioSource.clip.length;

		/// <summary>
		/// Returns the number of songs in the station.
		/// See also: <seealso cref="System.Collections.ObjectModel.ReadOnlyCollection{T}.Count"/>
		/// </summary>
		public int NumberOfSongs => playlistManager[Station].Songs.Count;

		private int previousStation, station, previousSong, song; // previousStation holds what station was the previous station. Station holds what the current station is.
		private bool pause;

		private readonly RadioPlaylistManager playlistManager; // Get's the RadioPlaylistManager.
		private AudioSource radioSource; // Creates an AudioSource that will be used to play songs.
		private MusicManagerPlay managerPlay; // Used to get the volume of the music.
		private Stack<int> previousSongsPlayed; // Holds the previous songs.

		/// <summary>
		/// The constructor gathers and makes other objects that will be used.
		/// </summary>
		private RadioPlayer() {
			// Get's the radio's playlist manager.
			playlistManager = RadioPlaylistManager.Singleton;

			CreateAudioSource();

			managerPlay = Find.MusicManagerPlay; // Grabs the MusicManagerPlay.

			previousSongsPlayed = new Stack<int>();

			station = 0; // Sets the station to default 0.
			previousStation = station;
			song = 0; // Set's to the first song.
			previousSong = song;

			Logging.DebugMessage( "RimRadioPlayer started.", true );
		}

		/// <summary>
		/// Update is called when MusicUpdate is called.
		/// </summary>
		public void Update() {
			if ( !RadioSourceAlive ) {
				Log.Warning( "The audio source is no more, recreating source." );

				CreateAudioSource();
			}

			// Checks to see if station has changed.
			if ( StationChanged ) {
				radioSource.Pause(); // Pauses radio if it has.

				previousSongsPlayed.Clear(); // Clears the stack of the songs.

				previousStation = station; // Assigns the previous station to the new one.
			}

			if ( SongChanged ) {
				previousSong = song;
			}

			radioSource.volume = managerPlay.CurSanitizedVolume; // Set's the volume of the audio source.

			try {
				CheckSong(); // Calls this method.
			} catch ( NullReferenceException e ) {
				Logging.Error( $"A NullReferenceException has occured.\nThe message states: {e.Message}\nStacktrace: {e.StackTrace}" );
			}
		}

		private void CreateAudioSource() {
			radioSource = new GameObject( "RimRadioPlayer" ) {
				transform = {
					parent = Find.Root.soundRoot.sourcePool.sourcePoolCamera.cameraSourcesContainer.transform
				}
			}.AddComponent<AudioSource>();

			radioSource.bypassEffects = true; // Makes it ignore all effects.
			radioSource.bypassListenerEffects = true; // Makes it ignore listern's effects.
			radioSource.bypassReverbZones = true; // Disables reverb.
			radioSource.priority = 0; // Makes it the highest priority.
		}

		private void CheckSong() {
			if ( !radioSource.isPlaying && !Pause ) {
				previousSongsPlayed.Push( song );

				if ( RimRadioSettings.shuffle ) {
					Shuffle();
				} else {
					NextInOrder();
				}

				radioSource.Play(); // Plays the clip.
			} else if ( radioSource.isPlaying && Pause ) {
				radioSource.Pause(); // Pauses the clip.
			}
		}

		#region Song Changing
		/// <summary>
		/// Plays the next song.
		/// </summary>
		public void NextSong() => radioSource.clip = null; // Removes the clip from the audio source to force new song.

		/// <summary>
		/// Plays a certian song.
		/// </summary>
		/// <param name="song">The number of that song from the list of songs.</param>
		public void SelectSong( int song ) {
			var previousSong = song; // Holds the current song number in case of fuck up.
			var prevClip = radioSource.clip; // Holds the previous song clip in case of fuck up.

			radioSource.Stop(); // Stops playing the audio clip.

			try {
				this.song = song; // Set's the current song to the one passed to this method.

				var songInfo = playlistManager[Station][song];

				Logging.DebugMessage( $"Got `{songInfo.ToString()}` from radio." );

				radioSource.clip = songInfo.Clip; // Set's the new clip to the song.

				previousSongsPlayed.Push( previousSong );
			} catch ( IndexOutOfRangeException e ) {
				Logging.Error( $"{e.GetType().FullName}: This message can usually be ignored, but please do report it on RimRadio's GitHub, GitLab, or Mod page.\nMessage: {e.Message} | Stacktrace: {e.StackTrace}" ); // Writes this error to the log.

				this.song = previousSong; // Reset's what was done.
				radioSource.clip = prevClip; // Reset's what was done.
			} catch ( Exception e ) {
				Logging.Error( $"{e.GetType().FullName}: Please report this to the mod's GitHub, GitLab, or Mod page.\nMessage: {e.Message} | Stacktrace: {e.StackTrace}" ); // Logs the exception out to the log file.

				this.song = previousSong; // Reset's what was done.
				radioSource.clip = prevClip; // Reset's what was done.
			} finally {
				radioSource.Play(); // Resumes or starts playing the song.
				SongChangedNotifier( this.song );
			}
		}

		/// <summary>
		/// Plays the previous song, if their is no previous song in it's list, it won't change anything.
		/// </summary>
		public void PreviousSong() {
			if ( previousSongsPlayed.Count == 0 ) { // Checks if the amount of previous songs played is 0.
				radioSource.time = 0f; // Reset's the current song back to the start.
				return; // Breaks out of the method.
			}

			song = previousSongsPlayed.Pop(); // Pops the int off of the stack.

			radioSource.Stop(); // Stops the audio source from playing.
			radioSource.clip = playlistManager[Station][song].Clip; // Assigns the clip to the audio source.
			radioSource.time = 0f; // Set's the time to 0.
			radioSource.Play(); // Plays it.

			SongChangedNotifier( song );
		}
		#endregion

		private void NextInOrder() {
			AudioClip clip;
			var station = playlistManager[Station];

			if ( station.Count < ++song ) {
				song = 0;
			}

			clip = station[song].Clip;

			radioSource.clip = clip;
		}

		private void Shuffle() {
			AudioClip newSong;

			newSong = GetRandomClip(); // Get's a new clip to play on the radio.

			radioSource.clip = newSong;
		}

		/// <summary>
		/// Get's a random clip from the station.
		/// </summary>
		/// <returns>An audio clip of that song.</returns>
		private AudioClip GetRandomClip() {
			var songIndex = 0;
			var keepGoing = true;
			var station = playlistManager[Station]; // Creates a new list based on the ReadOnly collection of songs.
			var clip = default( SongInfo );

			Logging.Message( $"Station num: {station}\nPlayable songs count: {station.Count}" );

			if ( station.Count > 1 ) {
				FindNewSong();
			} else {
				clip = station[0];
			}

			song = songIndex; // Assigns the song index to the song.

			SongChangedNotifier( song );

			Logging.Message( $"Is SongInfo null:\t{clip is null}\nIs AudioClip null:\t{clip?.Clip is null}" );

			return clip.Clip; // Returns the audio clip.

			void FindNewSong() {
				while ( keepGoing ) { // Keeps going for as long as the flag is up.
					var xDeep = 0;

					songIndex = UnityRandom.Range( 0, station.Count ); // Grabs a random value between 0, and the length - 1 of the songs.

					clip = station[songIndex]; // Grabs the clip.

					if ( !RimRadioSettings.ignoreTimeOfDay && !ValidPlayingTime( clip ) ) { // Checks to see if ignore time of day setting is false and is not a valid time to play.
						continue;
					}

					if ( !RimRadioSettings.ignoreAllowedSeasons && !ValidPlayingSeason( clip ) ) { // Checks to see if ignore allowed seasons is false and is not a valid season to play.
						continue;
					}

					foreach ( var prevSongIndex in previousSongsPlayed ) { // Loops through all the songs on the stack.

						Logging.Message( $"XDeep: {xDeep}\nNum of previous songs: {previousSongsPlayed.Count}\nStacktrace:\n{Environment.StackTrace}" );

						keepGoing = songIndex == prevSongIndex; // Checks to see if the indexes are the same and assigns the output to keepGoing.

						if ( keepGoing || ++xDeep >= station.Count / 3 ) { // Checks to see if keepGoing is true or if it has hit the end of the check.
							break; // Breaks out of the for each loop.
						}
					}
				}
			}
		}

		// Checks to see if it's a valid season to play.
		private bool ValidPlayingSeason( SongInfo song ) {
			var map = Find.AnyPlayerHomeMap ?? Find.CurrentMap; // Grabs the current map.
			var season = GenLocalDate.Season( map ); // Grabs the current season.

			return song.AllowedSeasons == null || song.AllowedSeasons.Count == 0 || map == null || song.AllowedSeasons.Contains( season ); // Returns if it is valid to play the song.
		}

		// Checks to see if it's a valid time to play the song.
		private bool ValidPlayingTime( SongInfo song ) {
			switch ( song.AllowedTimeOfDay ) { // Switches on the time of day aspect.
				case TimeOfDay.Day: // Case for if it is only allowed to be played during the day.
					return !IsNight(); // Returns if it is not night.
				case TimeOfDay.Night: // Case for if it is only allowed to be played during night.
					return IsNight(); // Returns if it is night.
				case TimeOfDay.Any: // Case for if it does not matter.
				default: // Default case.
					return true; // Returns true.
			}
		}

		private bool IsNight() {
			var map = Find.AnyPlayerHomeMap ?? Find.CurrentMap; // Grabs either the player home map or the current map.
			double timeOfDayPercent = GenLocalDate.DayPercent( map ); // Grabs the day percent for the map.

			return timeOfDayPercent < DAY_BREAK || NIGHT_FALL < timeOfDayPercent; // Returns if it is night time or not.
		}
	}
}