﻿using System.Collections;
using System.Collections.Generic;
using System.IO;

using RimRadio.Helpers;

using UnityEngine;

using Verse;

namespace RimRadio.Music {
	/// <summary>
	/// This public class stores the information of the radio station, while also generating the paths for each song.
	/// </summary>
	[StaticConstructorOnStartup]
	public class RadioStation : IEnumerable<SongInfo> {

		/// <summary>
		/// What unknown albums will have.
		/// </summary>
		public static Texture2D NoIconFound {
			get;
		}

		static RadioStation() {
			var rimRadioMod = LoadedModManager.GetMod<RimRadioMod>(); // Grabs this mod's mod class.
			var imagePath = Path.Combine( rimRadioMod.Content.RootDir, "Textures", "NoIcon.png" ); // Grabs the no icon image from the Textures folder.

			NoIconFound = LocalFileHelper.GetTextureWithoutWebRequest( imagePath );
		}

		/// <summary>
		/// Gets the name of the radio station.
		/// </summary>
		/// <value>
		/// The name of the radio station.
		/// </value>
		public string RadioName {
			get;
		}

		/// <summary>
		/// Gets the mod path.
		/// </summary>
		/// <value>
		/// The mod path.
		/// </value>
		public string ModPath {
			get;
		}

		public virtual int Count => Songs.Count;

		/// <summary>
		/// Gets the icon to be displayed.
		/// </summary>
		/// <value>
		/// The icon.
		/// </value>
		public Texture2D Icon {
			get;
		}

		/// <summary>
		/// Gets the list of songs that are to be played.
		/// </summary>
		/// <value>
		/// The songs that are to be played.
		/// </value>
		public virtual List<SongInfo> Songs {
			get;
		}

		/// <summary>
		/// Gets the <see cref="SongInfo"/> at the specified index.
		/// </summary>
		/// <value>
		/// The <see cref="SongInfo"/>.
		/// </value>
		/// <param name="index">The index.</param>
		/// <returns></returns>
		public virtual SongInfo this[int index] => Songs[index];

		/// <summary>
		/// Initializes a new instance of the <see cref="RadioStation"/> class.
		/// </summary>
		/// <param name="modPath">The path to the mod.</param>
		/// <param name="radioName">Name of the radio station.</param>
		/// <param name="icon">The icon of the radio station.</param>
		/// <param name="songs">The songs that are played on the station.</param>
		internal RadioStation( string modPath, string radioName, Texture2D icon, List<SongInfo> songs ) {
			ModPath = modPath;
			RadioName = radioName;
			Songs = songs;

			if ( icon is null ) { // Checks to see if icon is null.
				Icon = NoIconFound; // Assigns the icon to NoIconFound.
			} else {
				Icon = icon; // Assigns it to the icon passed to it.
			}
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="RadioStation"/> class.
		/// </summary>
		/// <param name="modPath">The path to the mod.</param>
		/// <param name="radioName">Name of the radio station.</param>
		/// <param name="icon">The icon of the radio station.</param>
		/// <param name="songs">The songs that are played on the station.</param>
		internal RadioStation( string modPath, string radioName, Texture2D icon, params SongInfo[] songs ) : this( modPath, radioName, icon, new List<SongInfo>( songs ) ) { }

		/// <summary>
		/// Initializes a new instance of the <see cref="RadioStation"/> class.
		/// </summary>
		/// <param name="modPath">The path to the mod.</param>
		/// <param name="radioName">Name of the radio station.</param>
		/// <param name="icon">The icon of the radio station.</param>
		internal RadioStation( string modPath, string radioName, Texture2D icon ) : this( modPath, radioName, icon, new List<SongInfo>() ) { }

		/// <summary>
		/// Initializes a new instance of the <see cref="RadioStation"/> class.
		/// </summary>
		/// <param name="modPath">The path to the mod.</param>
		/// <param name="radioName">Name of the radio station.</param>
		/// <param name="songs">The songs that are played on the station.</param>
		internal RadioStation( string modPath, string radioName, params SongInfo[] songs ) : this( modPath, radioName, NoIconFound, songs ) { }

		/// <summary>
		/// Initializes a new instance of the <see cref="RadioStation"/> class.
		/// </summary>
		/// <param name="modPath">The path to the mod.</param>
		/// <param name="radioName">Name of the radio station.</param>
		internal RadioStation( string modPath, string radioName ) : this( modPath, radioName, NoIconFound ) { }

		public IEnumerator<SongInfo> GetEnumerator() => Songs.GetEnumerator();

		IEnumerator IEnumerable.GetEnumerator() => Songs.GetEnumerator();

		/// <summary>
		/// <see cref="object"/>
		/// </summary>
		/// <returns></returns>
		public override string ToString() => $"Name{{\n\t{RadioName}\n}}Song Count{{\n\t{Songs.Count}\n}}Has Image{{\n\t{Icon is null}\n}}Pack Path{{\n\t'{ModPath}'\n}}";
	}
}