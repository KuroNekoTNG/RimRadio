﻿using System;
using System.Xml.Serialization;

namespace RimRadio.Music.Info {

	/// <summary>
	/// Used to represent a file that contains elements that will be used to identify a file and assin proper attributes to it.
	/// </summary>
	[Serializable, XmlRoot( "CrossReferenceFile" )]
	public class CrossRefStation : IEquatable<CrossRefStation> {

		/// <summary>
		/// The name of the station.
		/// </summary>
		[XmlElement( "StationName" )]
		public string StationName {
			get;
			set;
		}

		/// <summary>
		/// Represents where the icon is located at.
		/// </summary>
		[XmlElement( "IconPath" )]
		public string IconPath {
			get;
			set;
		}

		/// <summary>
		/// An array of songs.
		/// </summary>
		[XmlArray( "Songs" ), XmlArrayItem( "Song", typeof( CrossRefSong ) )]
		public CrossRefSong[] CrossRefSongs {
			get;
			set;
		}

		/// <summary>
		/// Compares two CrossRefStation.
		/// <seealso cref="IEquatable{T}.Equals(T)"/>
		/// </summary>
		/// <param name="other">The other object to compare against.</param>
		/// <returns>True if they are the same, otherwise false.</returns>
		public bool Equals( CrossRefStation other ) {
			if ( StationName == other.StationName && IconPath == other.IconPath && CrossRefSongs.Length == other.CrossRefSongs.Length ) {
				for ( var i = 0; i < CrossRefSongs.Length && i < other.CrossRefSongs.Length; ++i ) {
					if ( !CrossRefSongs[i].Equals( other.CrossRefSongs[i] ) ) {
						return false;
					}
				}

				return true;
			}

			return false;
		}
	}

	/// <summary>
	/// Represents the actual song files.
	/// </summary>
	[Serializable]
	public class CrossRefSong : IEquatable<CrossRefSong> {

		/// <summary>
		/// The relitive path to the file.
		/// </summary>
		[XmlElement( "SongPath" )]
		public string SongPath {
			get;
			set;
		}

		/// <summary>
		/// The title of the song.
		/// </summary>
		[XmlElement( "SongTitle" )]
		public string SongTitle {
			get;
			set;
		}

		/// <summary>
		/// The person or group that made the song.
		/// </summary>
		[XmlElement( "SongArtist" )]
		public string SongArtist {
			get;
			set;
		}

		/// <summary>
		/// The album the song is in.
		/// </summary>
		[XmlElement( "SongAlbum" )]
		public string SongAlbum {
			get;
			set;
		}

		/// <summary>
		/// Compares two CrossRefSongs.
		/// <seealso cref="IEquatable{T}.Equals(T)"/>
		/// </summary>
		/// <param name="other">The other object to compare against.</param>
		/// <returns>True if they are the same, otherwise false.</returns>
		public bool Equals( CrossRefSong other ) => SongPath == other.SongPath && SongTitle == other.SongTitle && SongArtist == other.SongArtist && SongAlbum == other.SongAlbum;
	}
}