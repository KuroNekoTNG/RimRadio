﻿using RimRadio.Music.UserInterface.DisplayStates;

using UnityEngine;

using Verse;

namespace RimRadio.Music.UserInterface
{
    /// <summary>
    /// This controls the radio
    /// </summary>
    public sealed class MusicPlayerWindow : Window
    {

        public static bool IsWindowShowing
        {
            get;
            private set;
        } = false;

        public static MusicPlayerWindow Singleton
        {
            get;
        } = new MusicPlayerWindow(RadioPlayer.Radio);

        /// <summary>
        /// The size of the window.
        /// </summary>
        public override Vector2 InitialSize => initialSize;

        /// <summary>
        /// FUCK YOU LUDEON! YOUR WINDOW SYSTEM IS SHIT!
        /// </summary>
        protected override float Margin => 0f;

        /// <summary>
        /// The custom margin of the window.
        /// </summary>
        internal float WindowMargin
        {
            get => margin;

            set
            {
                if (value >= 0f)
                {
                    margin = value;
                }
            }
        }

        // Positions and sizes.
        internal Vector2 initialSize;
        internal Vector2 position;

        private readonly ADisplayStateCore fullDisplay, miniDisplay, classicDisplay;

        private float margin;

        private ADisplayStateCore displayState;

        /// <summary>
        /// The constructor that assigns everything.
        /// </summary>
        /// <param name="radio">The radio player that it will talk to.</param>
        private MusicPlayerWindow(RadioPlayer radio) : base()
        {
            // Set's this Window to the Game's UI layer.
            layer = WindowLayer.GameUI; // Set's it to the game ui.

            resizeable = true; // Allows it to be resizeable.
            draggable = true; // Enables it to be draggable.
            preventCameraMotion = false; // Allows the camera to move.
            closeOnClickedOutside = false; // Prevents mouse clickout from closing the window.
            doCloseButton = false; // Removes the close button.
            doCloseX = false; // Removes the x button
            closeOnAccept = false; // Removes the close on accept.
            closeOnCancel = false; // Removes the close on cancel.
            doWindowBackground = false; // Disables the window background.
            drawShadow = false; // Disables the shadow.

            margin = 18f; // Set's the margins to 18f by default.

            fullDisplay = new FullDisplayState(this, radio); // Creates a new FullDisplayState.
            classicDisplay = new ClassicDisplayState(this, radio); // Creates the classic display state.
            miniDisplay = new MiniDisplayState(this, radio); // Creates the mini display state.

            displayState = RimRadioSettings.useClassic ? classicDisplay : fullDisplay; // Applies either the classic or the full display.

            displayState.PrepareWindow(); // Tells the display state to set up the display.

            Logging.DebugMessage("RadioDisplay created and showing.", true);
        }

        public void ToggleDisplaying()
        {
            IsWindowShowing = !IsWindowShowing;

            if (!IsWindowShowing)
            {
                Find.WindowStack.TryRemove(this, false);
            }
            else
            {
                Find.WindowStack.Add(this); // Adds this window to a stack of Windows.
            }
        }

        /// <summary>
        /// <see cref="Window.Notify_ResolutionChanged"/>
        /// </summary>
        public override void Notify_ResolutionChanged() => SetWindowRect();

        /// <summary>
        /// <see cref="Window.PreClose"/>
        /// </summary>
        public override void PreOpen()
        {
            base.PreOpen();
            SetWindowRect();
            displayState.PrepareWindow();
        }

        /// <summary>
        /// <see cref="Window.DoWindowContents(Rect)"/>
        /// </summary>
        public override void DoWindowContents(Rect inRect)
        {
            CreateWindowBackground(ref inRect); // Creates the window's background.

            inRect = inRect.ContractedBy(margin); // Computes a new inRect.

            /* Yo dawg, I heard you like GUI Groups.
			 * So I put a GUI Group in your GUI Group so you can CREATE A TOTALLY CUSOMIZABLE USER INTERFACE!
			 * Yes I am pissed about this because everything named window or what sounds like it relates to a window
			 * doesn't modify the window or isn't modifyable because it is either read-only or a constant.
			 * Fuck you Ludeon for not making a window more customizable, and fuck you for locking down what a Window
			 * looks like! */
            GUI.BeginGroup(inRect); // Begins a new GUI Group. Why? So margins work.

            displayState.DoWindowStateContent(inRect.AtZero()); // Forwards the call to the state.

            GUI.EndGroup(); // End's the new GUI Group.
        }

        internal void ChangeDisplayState()
        {
            if (displayState is MiniDisplayState || displayState is null)
            { // Checks to see if the mini display is the current state.
                displayState = RimRadioSettings.useClassic ? classicDisplay : fullDisplay; // Applies either the classic or the default depending on the user's settings.
            }
            else
            {
                if (RimRadioSettings.miniHides)
                {
                    ToggleDisplaying();
                }
                else
                {
                    displayState = miniDisplay; // Set's the state to the mini display.
                }
            }

            displayState.PrepareWindow(); // Calls this to prepare the Window for a change.

            SetWindowRect(); // Calls this to reset it's position and size.
        }

        internal void SetWindowRect() => windowRect = new Rect(position, InitialSize); // Creates a new rect with the window position and inital size.

        public override void WindowUpdate()
        {
            base.WindowUpdate(); // Calls the base method for window update.

            RimRadioSettings.UpdateDefaultPos(); // Updates the default position in settings.

            CheckBounds(); // Ensures the window does not shrink to below a certian size.

            UpdateSettings(); // Updates the settings.
        }

        private void CheckBounds()
        {

            if (displayState == miniDisplay)
            { // Checks to see if the current state is the mini display state.
                return; // Breaks out of the method.
            }

            if (windowRect.height < RimRadioSettings.DefaultWindowSize.y)
            { // Checks to see if the window is smaller than the default winow size.
                windowRect.height = RimRadioSettings.DefaultWindowSize.y; // Set's the height to be the default size.
            }

            if (windowRect.width < RimRadioSettings.DefaultWindowSize.x)
            { // Checks to see if the window is smaller than the width.
                windowRect.width = RimRadioSettings.DefaultWindowSize.x; // Set's the width to be the default size/
            }
        }

        internal void UpdateSettings()
        {
            var change = false;

            if (displayState == miniDisplay)
            { // Checks to see if the current state is the mini display.
                return; // Breaks out of the method.
            }

            if (RimRadioSettings.winPos.x != windowRect.x)
            { // Checks to see if the x position is different.
                change = true;
                RimRadioSettings.winPos.x = windowRect.x; // Copies over the size.
            }

            if (RimRadioSettings.winPos.y != windowRect.y)
            { // Checks to see if the y position is different.
                change = true;
                RimRadioSettings.winPos.y = windowRect.y; // Copies the y position over.
            }

            if (RimRadioSettings.winSize.x != windowRect.width)
            { // Checks to see if the width is different.
                change = true;
                RimRadioSettings.winSize.x = windowRect.width; // Copies the width to the settings.
            }

            if (RimRadioSettings.winSize.y != windowRect.height)
            { // Checks to see if the height is different.
                change = true;
                RimRadioSettings.winSize.y = windowRect.height; // Copies over the height to settings.
            }

            if (change)
            { // Checks if anything has changed.
                RimRadioMod.RimRadioInstence.WriteSettings(); // Writes the settings. 
            }
        }

        private void CreateWindowBackground(ref Rect inRect)
        {
            Widgets.DrawRectFast(inRect, RimRadioSettings.PlayerColor); // Creates a new background with the player set color.
            Widgets.DrawBox(inRect); // Creates a box around the window.
        }
    }
}