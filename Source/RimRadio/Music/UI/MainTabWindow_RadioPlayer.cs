﻿using RimWorld;

using UnityEngine;

using Verse;

namespace RimRadio.Music.UserInterface {
	public class MainTabWindow_RadioPlayer : MainTabWindow {

		public const string SONG_CHANGED_KEY = "SongChangeNotification";

		private MusicPlayerWindow player;
		private RadioPlayer radio;

		public override Vector2 RequestedTabSize => new Vector2( 0, 0 );

		public MainTabWindow_RadioPlayer() : base() {
			closeOnClickedOutside = false;
			closeOnAccept = false;
			doCloseButton = false;
			doCloseX = false;

			do {
				radio = RadioPlayer.Radio;

				Logging.Message( $"Radio Status: {radio is null}" );
			} while ( radio is null );

			do {
				player = MusicPlayerWindow.Singleton;

				Logging.Message( $"Player Status: {player is null}" );
			} while ( player is null );

			radio.SongChangedNotifier += SongChanged;
		}

		public override void PreOpen() => player.ToggleDisplaying();

		public override void DoWindowContents( Rect inRect ) => Close();

		private void SongChanged( int song ) {
			if ( !MusicPlayerWindow.IsWindowShowing ) {
				Messages.Message( SONG_CHANGED_KEY.Translate( radio.CurrentSongTitle, radio.CurrentSongArtist ), MessageTypeDefOf.NeutralEvent, false );
			}
		}
	}
}
