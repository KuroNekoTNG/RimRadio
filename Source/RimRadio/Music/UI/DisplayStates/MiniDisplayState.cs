﻿using System;

using UnityEngine;

using Verse;

namespace RimRadio.Music.UserInterface.DisplayStates {
	internal sealed class MiniDisplayState : ADisplayStateCore {

		/// <summary>
		/// The size of the station icon for the mini display.
		/// </summary>
		private const float MINI_IMAGE_SIZE = 32f;

		/// <summary>
		/// The amount of space between the text and the station image.
		/// </summary>
		private const float TEXT_IMAGE_BUFFER = 8f;

		/// <summary>
		/// The height of the mini window.
		/// </summary>
		private const float MINI_WINDOW_WIDTH = 256f;

		private sbyte stage;
		private float timePassed;
		private string visableText;

		private Rect imageRect, textRect;

		public MiniDisplayState( MusicPlayerWindow playerWindow, RadioPlayer radio ) : base( playerWindow, radio ) {
			playerWindow.resizeable = false; // Set's resizeable to false.

			stage = 0; // Defaults the stage to -1.
			timePassed = RimRadioSettings.waitTime; // Defaults it to the trigger amount.

			imageRect = new Rect( 0f, 0f, MINI_IMAGE_SIZE, MINI_IMAGE_SIZE ); // Creates a new rect that will represent the area that the image will be at.
			textRect = new Rect( MINI_IMAGE_SIZE + TEXT_IMAGE_BUFFER, 4f, MINI_WINDOW_WIDTH, MINI_IMAGE_SIZE ); // Creates a text rect.
		}

		internal override void DoWindowStateContent( Rect inRect ) {
			bool stationButtonPressed;
			var stationIcon = RadioPlaylistManager.Singleton[Radio.Station].Icon; // Grabs the icon from the station.

			stationButtonPressed = Widgets.ButtonImage( imageRect, stationIcon ); // Creates a new button image and grabs if it has been pressed.

			timePassed += Time.deltaTime; // Adds the delta time to the time passed.

			if ( timePassed >= RimRadioSettings.waitTime ) { // Checks to see if the appropiate amount of time has passed.
				if ( 3 <= ++stage ) { // Incriments stage by 1 and checks to see if the stage went about it's bounds.
					stage = 0; // Set's it back to 0.
				}

				timePassed = 0f; // Reset's the time to 0.
			}

			GrabVisableText(); // Grabs the next text item.

			Text.Font = GameFont.Small; // Makes the text stays the same size.

			Widgets.Label( textRect, visableText );

			if ( stationButtonPressed ) { // Checks if the button has been pressed.
				MusicPlayerWindow.ChangeDisplayState(); // Changes the display state of the music player.

				timePassed = RimRadioSettings.waitTime; // Reset's the time passed to the constant.
				stage = -1; // Reset's the stage to -1.
			}
		}

		internal override void PrepareWindow() {
			var newSize = new Vector2( MINI_WINDOW_WIDTH, MINI_IMAGE_SIZE ); // Creates a new window with this size.
			var position = new Vector2( Screen.width - newSize.x, 0 ); // Makes the mini display go into the upper right conor.

			MusicPlayerWindow.initialSize = newSize; // Set's it as a new size;
			MusicPlayerWindow.resizeable = false; // Denys resizing the mini player.
			MusicPlayerWindow.doWindowBackground = false; // Disables the mini-player's background.
			MusicPlayerWindow.WindowMargin = 1f; // Set's the margins to 1f.
			MusicPlayerWindow.position = position; // Set's the position.

			WindowOutOfBounds();
		}

		private void GrabVisableText() {
			switch ( stage ) { // Switches on the stage after incrimenting it by 1.
				case 0:
					visableText = Radio.CurrentSongTitle; // Grabs the title.
					return;
				case 1:
					visableText = Radio.CurrentSongArtist; // Grabs the artist.
					return;
				case 2:
					visableText = Radio.CurrentSongAlbum; // Grabs the album.
					return;
				default:
					throw new Exception( "Unexpected change to stage." ); // This should never get hit.
			}
		}
	}
}