﻿using System.IO;

using RimRadio.Helpers;

using UnityEngine;

using Verse;

namespace RimRadio.Music.UserInterface.DisplayStates {
	/* TODO: Rewrite code to make it more functional and readable.
	 * 
	 * Well, better late then never, and even now since RimWorld uses .NET Framework 4.X because it uses Unity 2019!
	 * Just not looking forward to doing this, but let's FUCKING DO THIS SHIT BITCH!
	 * Got STONEBANK - The Only One playing! */
	 [StaticConstructorOnStartup]
	internal sealed class FullDisplayState : ADisplayStateCore {

		#region UI Constants
		public const float STATION_ICON_SIZE = 64f;
		public const float STATION_MINI_ICON_SIZE = 16f;
		public const float BUTTON_LOCATION_Y = 95f;
		public const float SONG_TEXT_X_OFFSET = 5f;
		public const float SONG_SELECTION_HEIGHT_OFFSET = 5f;
		public const float SCROLL_BUFFER_SIZE = 4f;
		#endregion

		public static readonly Color BASE_COLOR = new Color( 1f, 1f, 1f, 1f );
		public static readonly Color MOUSE_OVER = new Color( 1f, 1f, 1f, 0.25f );

		#region Texture Path
		private static readonly string PLAY_TEXTURE = Path.Combine( RimRadioMod.TextureFolder, "Play.png" );
		private static readonly string PAUSE_TEXTURE = Path.Combine( RimRadioMod.TextureFolder, "Pause.png" );
		private static readonly string PREVIOUS_TEXTURE = Path.Combine( RimRadioMod.TextureFolder, "Previous.png" );
		private static readonly string FORWARD_TEXTURE = Path.Combine( RimRadioMod.TextureFolder, "Forward.png" );
		#endregion

		#region Textures
		private static readonly Texture2D playTexture = LocalFileHelper.GetTextureWithoutWebRequest( PLAY_TEXTURE );
		private static readonly Texture2D pauseTexture = LocalFileHelper.GetTextureWithoutWebRequest( PAUSE_TEXTURE );
		private static readonly Texture2D previousTexture = LocalFileHelper.GetTextureWithoutWebRequest( PREVIOUS_TEXTURE );
		private static readonly Texture2D forwardTexture = LocalFileHelper.GetTextureWithoutWebRequest( FORWARD_TEXTURE );
		#endregion

		private float buttonOffset = 1.2f;

		private Vector2 songListScrollPos;
		private Vector2 stationListScrollPos;
		private Rect rewindButtonRect;
		private Rect playButtonRect;
		private Rect skipButtonRect;
		private Rect switchButtonRect;

		public FullDisplayState( MusicPlayerWindow playerWindow, RadioPlayer Radio ) : base( playerWindow, Radio ) {
			songListScrollPos = new Vector2(); // Creates a new Vector2 for the scroll position.
			stationListScrollPos = new Vector2(); // Creates a new Vector2 for the scroll position.

			CreateButtonRects();

			Radio.SongChangedNotifier += SongChanged;

			void CreateButtonRects() {
				// Creates the size of the buttons.
				var mediaButtonSize = new Vector2( 32f, 32f );
				// Media control button locations.
				var rewindButtonLocation = new Vector2( STATION_ICON_SIZE * buttonOffset, 0f );
				var playButtonLocation = new Vector2( rewindButtonLocation.x + mediaButtonSize.x, 0f );
				var skipButtonLocation = new Vector2( playButtonLocation.x + mediaButtonSize.x, 0f );
				// The toggle between the mini and full display size and location.
				var displayStateToggleSize = new Vector2( mediaButtonSize.x * 3, mediaButtonSize.y );
				var displayStateToggleLocation = new Vector2( rewindButtonLocation.x, mediaButtonSize.y + buttonOffset );

				rewindButtonRect = new Rect( rewindButtonLocation, mediaButtonSize );
				playButtonRect = new Rect( playButtonLocation, mediaButtonSize );
				skipButtonRect = new Rect( skipButtonLocation, mediaButtonSize );
				switchButtonRect = new Rect( displayStateToggleLocation, displayStateToggleSize );
			}
		}

		internal override void PrepareWindow() {
			MusicPlayerWindow.initialSize = RimRadioSettings.winSize; // Set's the window to the new window style.
			MusicPlayerWindow.position = RimRadioSettings.winPos;
			MusicPlayerWindow.resizeable = true;
			MusicPlayerWindow.WindowMargin = 9f;
		}

		internal override void DoWindowStateContent( Rect inRect ) {
			Text.Font = GameFont.Small; // Set's the game font to small.

			WindowOutOfBounds();

			CreateStationIcon( inRect );
			CreateSeeker( inRect );
			CreateButtons();
			CreateSongSelectionList( inRect );
			CreateStationSelection( inRect );

			MusicPlayerWindow.UpdateSettings();
		}

		private void SongChanged( int newValue ) {
			Logging.DebugMessage( "Kicking scroll position." );

			songListScrollPos.y = newValue * ( Text.LineHeight + buttonOffset ); // Set's the y to a new position.
		}

		private void CreateSongSelectionList( Rect inRect ) {
			var station = RadioPlaylistManager.Singleton[Radio.Station];
			var viewRect = new Rect( inRect );
			var height = ( Text.LineHeight + buttonOffset ) * station.Count;

			viewRect.yMin += 16f + STATION_ICON_SIZE * buttonOffset + SONG_SELECTION_HEIGHT_OFFSET;

			var container = new Rect( 0f, 0f, viewRect.width, height );

			Widgets.DrawRectFast( viewRect, RimRadioSettings.PlayerScrollColor );
			Widgets.DrawBox( viewRect );

			viewRect = viewRect.ContractedBy( SCROLL_BUFFER_SIZE );

			container.width = viewRect.width;

			if ( container.height > viewRect.height ) {
				container.width -= 16f;
			}

			Widgets.BeginScrollView( viewRect, ref songListScrollPos, container );

			for ( int i = 0; i < station.Count; ++i ) {
				var yPos = i * ( Text.LineHeight + buttonOffset );
				var textRect = new Rect( viewRect.x, yPos, container.width, Text.LineHeight );

				if ( i % 2 == 0 && i != Radio.CurrentSong ) {
					Widgets.DrawHighlight( textRect );
				} else if ( i == Radio.CurrentSong ) {
					Widgets.DrawRectFast( textRect, RimRadioSettings.HighlightColor );
				}

				if ( Widgets.ButtonText( textRect, station[i].ToString(), false, true ) ) {
					Logging.DebugMessage( $"Selecting song `{station[i].ToString()}` which has the index of `{i}`." );

					Radio.SelectSong( i );
				}
			}

			Widgets.EndScrollView();
		}

		private void CreateSeeker( Rect inRect ) {
			float yPosition;
			// Grabs the text size.
			var curTimeTextSize = Text.CalcSize( Radio.CurrentTime );
			var totalTimeTextSize = Text.CalcSize( Radio.LengthOfSong );
			// Creates the rects.
			Rect curTimeRect;
			Rect totalTimeRect;
			Rect seekerRect;
			// Calculates where on the y axis these components will be located.
			yPosition = ( BUTTON_LOCATION_Y - STATION_ICON_SIZE ) / 2 - ( curTimeTextSize.y / 2 ) + STATION_ICON_SIZE;
			// Does calculations for their sizes and dynamic positions, and creates a rect.
			curTimeRect = new Rect( 0f, yPosition, curTimeTextSize.x, curTimeTextSize.y );
			totalTimeRect = new Rect(
				inRect.width - totalTimeTextSize.x,
				yPosition,
				totalTimeTextSize.x,
				totalTimeTextSize.y
			);
			seekerRect = new Rect(
				curTimeRect.xMax + buttonOffset * 2,
				yPosition + ( GenUI.HorizontalSliderHeight / 4 ),
				totalTimeRect.xMin - curTimeRect.xMax - ( buttonOffset * 4 ),
				totalTimeRect.height
			);

			Widgets.Label( curTimeRect, Radio.CurrentTime );
			Widgets.Label( totalTimeRect, Radio.LengthOfSong );
			Widgets.HorizontalSlider( seekerRect, Radio.FloatTime, 0f, Radio.FloatLengthTime );
		}

		private void CreateStationSelection( Rect inRect ) {
			var stationManager = RadioPlaylistManager.Singleton;
			var lineSize = ( STATION_MINI_ICON_SIZE > Text.LineHeight ? STATION_MINI_ICON_SIZE : Text.LineHeight ) + buttonOffset;
			var container = new Rect(
				0f, 0f,
				inRect.width - ( STATION_ICON_SIZE * buttonOffset * 3 ) + 15f,
				stationManager.Count * lineSize
			);
			var viewRect = new Rect( inRect ) {
				yMax = STATION_ICON_SIZE
			};

			viewRect.xMin += 32f + STATION_ICON_SIZE * buttonOffset * 2;

			Widgets.DrawRectFast( viewRect, RimRadioSettings.PlayerScrollColor );
			Widgets.DrawBox( viewRect );

			viewRect = viewRect.ContractedBy( SCROLL_BUFFER_SIZE );
			container.width = viewRect.width;

			if ( container.height > viewRect.height ) {
				container.width -= 16f;
			}

			Widgets.BeginScrollView( viewRect, ref stationListScrollPos, container );

			for ( int i = 0; i < stationManager.Count; ++i ) {
				var yPos = i * lineSize;
				var icon = new Rect(
					0f,
					yPos,
					STATION_MINI_ICON_SIZE,
					STATION_MINI_ICON_SIZE
				);
				var text = new Rect(
					STATION_MINI_ICON_SIZE,
					yPos,
					container.width - STATION_MINI_ICON_SIZE * buttonOffset,
					STATION_MINI_ICON_SIZE
				);
				var fullRow = new Rect(
					0f,
					yPos,
					container.width,
					STATION_MINI_ICON_SIZE
				);

				if ( i == Radio.Station ) {
					Widgets.DrawRectFast( fullRow, RimRadioSettings.HighlightColor );
				} else if ( i % 2 == 0 ) {
					Widgets.DrawHighlight( fullRow );
				}

				if ( Widgets.ButtonImage( icon, stationManager[i].Icon ) || Widgets.ButtonText( text, stationManager[i].RadioName, false ) ) {
					Radio.Station = i;
				}
			}

			Widgets.EndScrollView();
		}

		private void CreateButtons() {
			bool previous, playPause, next, @switch;

			// Grabs the translation of each string and stores them.
			var switchButtonString = RimRadioSettings.miniHides ? "HideButton".Translate() : "SwitchButton".Translate();

			// Grabs the bool from the button.
			previous = Widgets.ButtonImage( rewindButtonRect, previousTexture );

			if ( Radio.Pause ) {
				playPause = Widgets.ButtonImage( playButtonRect, playTexture );
			} else {
				playPause = Widgets.ButtonImage( playButtonRect, pauseTexture );
			}

			next = Widgets.ButtonImage( skipButtonRect, forwardTexture );
			@switch = Widgets.ButtonText( switchButtonRect, switchButtonString );

			if ( previous ) {
				Radio.PreviousSong();
			} else if ( playPause ) {
				Radio.Pause = !Radio.Pause;
			} else if ( next ) {
				Radio.NextSong();
			} else if ( @switch ) {
				MusicPlayerWindow.ChangeDisplayState();
			}
		}

		private void CreateStationIcon( Rect inRect ) {
			// Creates a new rect for the icon.
			var iconRect = new Rect( inRect.xMin, inRect.yMin, STATION_ICON_SIZE, STATION_ICON_SIZE );
			// Grabs the icon of the station.
			var iconText = RadioPlaylistManager.Singleton[Radio.Station].Icon;

			Widgets.DrawTextureFitted( iconRect, iconText, 1f );
		}
	}
}