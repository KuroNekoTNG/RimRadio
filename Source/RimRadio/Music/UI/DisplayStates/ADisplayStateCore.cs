﻿using UnityEngine;

namespace RimRadio.Music.UserInterface.DisplayStates {
	/// <summary>
	/// An <see langword="abstract"/> class that allows for common things to not be retyped.
	/// </summary>
	internal abstract class ADisplayStateCore {

		/// <summary>
		/// The music player window.
		/// </summary>
		protected MusicPlayerWindow MusicPlayerWindow {
			get;
		}

		/// <summary>
		/// The radio, aka, the thing that plays the songs.
		/// </summary>
		protected RadioPlayer Radio {
			get;
		}

		/// <summary>
		/// This constructor set's everything up.
		/// </summary>
		/// <param name="playerWindow">The window that holds the controls.</param>
		/// <param name="radio">The thing that plays and manages which song is playing.</param>
		internal ADisplayStateCore( MusicPlayerWindow playerWindow, RadioPlayer radio ) {
			MusicPlayerWindow = playerWindow;
			Radio = radio;
		}

		/// <summary>
		/// This is called by the player window.
		/// </summary>
		/// <param name="inRect">The inside of the window.</param>
		internal abstract void DoWindowStateContent( Rect inRect );

		/// <summary>
		/// This is called when the radio player window is changing.
		/// </summary>
		internal abstract void PrepareWindow();

		protected void WindowOutOfBounds() {
			var winRect = MusicPlayerWindow.windowRect; // Makes it less typing.

			// Checks to see if window is outside the bounds of the screen.
			// This is useful so the window does not become inaccessable due to shrinking the game.
			if ( winRect.x < 0f ) {
				winRect.x = 0f;
			} else if ( winRect.x + winRect.width >= Screen.width ) {
				winRect.x = Screen.width - winRect.width;
			}

			if ( winRect.y < 0f ) {
				winRect.y = 0f;
			} else if ( winRect.y + winRect.height >= Screen.height ) {
				winRect.y = Screen.height - winRect.height;
			}

			// Reassigns the new rect so it takes affect.
			MusicPlayerWindow.windowRect = winRect;
		}
	}
}