﻿using System.Collections.Generic;

using UnityEngine;

using Verse;

namespace RimRadio.Music.UserInterface.DisplayStates {
	// NO ONE DARE TOUCH THIS CLASS!
	// Me from future, doesn't seem that bad. But yea, I don't think I will, mostly because there is nothing that I could see optimizing off the bat.
	internal class ClassicDisplayState : ADisplayStateCore {

		public const float STATION_ICON_SIZE = 64f;
		public const float BUTTON_LOCATION_Y = 106.25f;
		public const float SONG_TEXT_X_OFFSET = 5f;
		public const float SONG_SELECTION_HEIGHT_OFFSET = 5f;

		public static readonly Color BASE_COLOR = new Color( 1f, 1f, 1f, 1f );
		public static readonly Color MOUSE_OVER = new Color( 1f, 1f, 1f, 0.25f );
		public static readonly Vector2 REQUESTED_WINDOW_SIZE = new Vector2( 400f, 255f );

		private const int NUMBER_OF_BUTTONS = 4; // Constant for the number of buttons below the seeker. Change this value if adding another button there.

		private float buttonOffset = 1.2f;
		private float textHeight;

		private Vector2 scrollPos;
		private StationSelection stationSelection;
		private List<Rect> songRects;

		public ClassicDisplayState( MusicPlayerWindow playerWindow, RadioPlayer Radio ) : base( playerWindow, Radio ) {
			scrollPos = new Vector2(); // Creates a new Vection2 for the scroll position.
			stationSelection = new StationSelection( Radio );
			songRects = new List<Rect>();
		}

		internal override void PrepareWindow() {
			MusicPlayerWindow.initialSize = REQUESTED_WINDOW_SIZE; // Set's the window to the new window style.
			MusicPlayerWindow.resizeable = true;
			MusicPlayerWindow.WindowMargin = 18f;
		}

		internal override void DoWindowStateContent( Rect inRect ) {
			Text.Font = GameFont.Small; // Set's the game font to small.

			CreateStationIcon( inRect );
			CreateSongInformationLabel( inRect );
			CreateSeeker( inRect );
			CreateButtons( inRect );
			CreateSongSelectionList( inRect );
		}

		private void CreateSongSelectionList( Rect inRect ) {
			var station = RadioPlaylistManager.Singleton[Radio.Station]; // Grabs the radio station that is selected.
			var songSelectionArea = inRect; // Creates a duplicate of the inRect.
			var viewRect = new Rect( 0f, 0f, inRect.width - 16f, Radio.NumberOfSongs * ( textHeight + buttonOffset ) ); // Creates a view rect for the scroll view.

			textHeight = Text.CalcHeight( "Sample", inRect.width ); // Gets the height that will be for all the buttons.

			songSelectionArea.yMin += BUTTON_LOCATION_Y * buttonOffset + SONG_SELECTION_HEIGHT_OFFSET; // Set's the yMin to lower the height.

			Widgets.BeginScrollView( songSelectionArea, ref scrollPos, viewRect ); // Begins the scroll view.

			for ( var i = 0; i < Radio.NumberOfSongs; ++i ) { // Loops through all the songs.
				var yViewPosition = viewRect.y + i * ( textHeight + buttonOffset ); // Calculates the y position for the button.
				Rect textRect;

				if ( Radio.StationChanged || i < songRects.Count ) { // Checks to see if either the station has changed, or there is no song rect at i.
					textRect = songRects[i];
				} else {
					textRect = new Rect( viewRect.x, yViewPosition, viewRect.width, textHeight ); // Creates a rect that the button will be at.
					songRects.Add( textRect );
				}

				if ( i % 2 == 0 && i != Radio.CurrentSong ) { // Checks to see if it is every other button.
					Widgets.DrawHighlight( textRect ); // Highlights every other button.
				}

				if ( Widgets.ButtonText( textRect, station[i].ToString(), false, true ) ) { // Creates the button and checks to see if it has been selected.
					Radio.SelectSong( i ); // Changes the song to the new button.
				}
			}

			Widgets.EndScrollView(); // Ends the scroll view.
		}

		private void CreateSeeker( Rect inRect ) {
			float yPosition;
			// Grabs the text size.
			var curTimeTextSize = Text.CalcSize( Radio.CurrentTime );
			var totalTimeTextSize = Text.CalcSize( Radio.LengthOfSong );
			// Creates the rects.
			Rect curTimeRect;
			Rect totalTimeRect;
			Rect seekerRect;
			// Calculates where on the y axis these components will be located.
			yPosition = ( BUTTON_LOCATION_Y - STATION_ICON_SIZE ) / 2 - ( curTimeTextSize.y / 2 ) + STATION_ICON_SIZE;
			// Does calculations for their sizes and dynamic positions, and creates a rect.
			curTimeRect = new Rect( 0f, yPosition, curTimeTextSize.x, curTimeTextSize.y );
			totalTimeRect = new Rect( inRect.width - totalTimeTextSize.x, yPosition, totalTimeTextSize.x, totalTimeTextSize.y );
			seekerRect = new Rect( curTimeRect.xMax + buttonOffset * 2, yPosition + ( GenUI.HorizontalSliderHeight / 4 ), ( totalTimeRect.xMin - curTimeRect.xMax ) - ( buttonOffset * 4 ), totalTimeRect.height );

			Widgets.Label( curTimeRect, Radio.CurrentTime );
			Widgets.Label( totalTimeRect, Radio.LengthOfSong );
			Widgets.HorizontalSlider( seekerRect, Radio.FloatTime, 0f, Radio.FloatLengthTime );
		}

		private void CreateSongInformationLabel( Rect inRect ) {
			var textStartX = inRect.xMin + STATION_ICON_SIZE + SONG_TEXT_X_OFFSET; // Creates the starting position for the text on the x position.
			var textWidth = textStartX - inRect.width; // Creates a standard with.

			// Creates vector 2 positions.
			var songTitlePos = new Vector2( textStartX, inRect.yMin ); // Creates a new vector 2 that represents where the song title pos goes.
			var songAlbumPos = new Vector2( textStartX, songTitlePos.y + Text.CalcHeight( Radio.CurrentSongTitle, textWidth ) ); // Creates a new vector 2 like before, except it goes down the amount the previous text size is.
			var songArtistPos = new Vector2( textStartX, songAlbumPos.y + Text.CalcHeight( Radio.CurrentSongAlbum, textWidth ) ); // Creates a new vector 2 like before, except it goes down the amount the previous text size is.

			// Creates rects that will represent where the text is located at.
			var songTitleRect = new Rect( songTitlePos, Text.CalcSize( Radio.CurrentSongTitle ) );
			var songAlbumRect = new Rect( songAlbumPos, Text.CalcSize( Radio.CurrentSongAlbum ) );
			var songArtistRect = new Rect( songArtistPos, Text.CalcSize( Radio.CurrentSongArtist ) );

			// Creates the labels.
			Widgets.Label( songTitleRect, Radio.CurrentSongTitle );
			Widgets.Label( songAlbumRect, Radio.CurrentSongAlbum );
			Widgets.Label( songArtistRect, Radio.CurrentSongArtist );
		}

		private void CreateButtons( Rect inRect ) {
			bool previous, playPause, next, @switch;
			var buttonSpace = inRect.width / ( NUMBER_OF_BUTTONS + 1 ); // The amount of space between a button. 1 is added to count spaces.

			// Grabs the translation of each string and stores them.
			var playPauseButtonString = Radio.Pause ? "PlayButton".Translate() : "PauseButton".Translate();
			var previousButtonString = "PreviousButton".Translate();
			var nextButtonString = "NextButton".Translate();
			var switchButtonString = "SwitchButton".Translate();

			// Creates the size of the buttons.
			var buttonSize = new Vector2( ( inRect.width / NUMBER_OF_BUTTONS ) - ( 1 / buttonOffset ), 25f );

			buttonSpace = ( buttonSpace / buttonSize.x ) * buttonOffset; // Divides by the button size and times it by the button offset.

			// Creates the rects for the buttons.
			var previousButton = new Rect( new Vector2( inRect.xMin, BUTTON_LOCATION_Y ), buttonSize );
			var playPauseButton = new Rect( new Vector2( previousButton.xMax + buttonSpace, BUTTON_LOCATION_Y ), buttonSize );
			var nextButton = new Rect( new Vector2( playPauseButton.xMax + buttonSpace, BUTTON_LOCATION_Y ), buttonSize );
			var switchButton = new Rect( new Vector2( nextButton.xMax + buttonSpace, BUTTON_LOCATION_Y ), buttonSize );

			// Checks to see if the previous button label is too long for the button, and if so, get the short hand.
			previousButtonString = Text.CalcSize( previousButtonString ).x > buttonSize.x ? "PreviousButtonShort".Translate() : previousButtonString;

			// Grabs the bool from the button.
			previous = Widgets.ButtonText( previousButton, previousButtonString );
			playPause = Widgets.ButtonText( playPauseButton, playPauseButtonString );
			next = Widgets.ButtonText( nextButton, nextButtonString );
			@switch = Widgets.ButtonText( switchButton, switchButtonString );

			// Checks to see which button was pressed.
			if ( previous ) { // Checks if previous was pressed.
				Radio.PreviousSong(); // Tells the radio to go back to the previous song.
			} else if ( playPause ) { // Checks if the play/pause button was pressed.
				Radio.Pause = !Radio.Pause; // Inverts the pause bool.
			} else if ( next ) { // Checks if the next song button was pressed.
				Radio.NextSong(); // Tells the radio to go to the next song.
			} else if ( @switch ) { // Checks to see if the switch state button was pressed.
				MusicPlayerWindow.ChangeDisplayState(); // Tells the music player window to switch.
			}
		}

		private void CreateStationIcon( Rect inRect ) {
			bool changeStation;
			var iconRect = new Rect( inRect.xMin, inRect.yMin, STATION_ICON_SIZE, STATION_ICON_SIZE ); // Creates a new rect for the icon.
			var iconText = RadioPlaylistManager.Singleton[Radio.Station].Icon; // Grabs the icon of the station.

			changeStation = Widgets.ButtonImage( iconRect, iconText, BASE_COLOR, MOUSE_OVER ); // Creates the button with the rect, image, and the 2 colors.

			if ( changeStation && !Find.WindowStack.IsOpen<StationSelection>() ) { // Checks if the button has been pressed.
				Find.WindowStack.Add( stationSelection ); // Adds it to the stack.
			}
		}
	}
}
