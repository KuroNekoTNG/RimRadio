﻿
using UnityEngine;

using Verse;

namespace RimRadio.Music.UserInterface {
	internal sealed class StationSelection : Window {

		private readonly bool[] buttons;

		private readonly RadioPlaylistManager radioPlaylist;
		private readonly RadioPlayer radio;

		public override Vector2 InitialSize => new Vector2( 300f, 500f );

		public Vector2 Position => new Vector2( ( Verse.UI.screenWidth / 2 ) - ( InitialSize.x / 2 ), ( Verse.UI.screenHeight / 2 ) - ( InitialSize.y / 2 ) );

		public StationSelection( RadioPlayer radio ) {
			radioPlaylist = RadioPlaylistManager.Singleton;
			this.radio = radio;
			buttons = new bool[radioPlaylist.Count];

			closeOnClickedOutside = true;
			doCloseX = true;
			focusWhenOpened = true;
			layer = WindowLayer.Dialog;
			onlyOneOfTypeAllowed = true;
			preventCameraMotion = true;
			forcePause = true;
		}

		public override void PostOpen() {
			base.PostOpen();
			windowRect = new Rect( Position, InitialSize );
		}

		public override void DoWindowContents( Rect inRect ) {
			var widgetRow = new WidgetRow( 0, 0, UIDirection.RightThenDown, InitialSize.x - ( Margin * 2 ) );

			for ( var i = 0; i < radioPlaylist.Count; ++i ) {
				buttons[i] = widgetRow.ButtonIcon( radioPlaylist[i].Icon, $"Radio Station: {radioPlaylist[i].RadioName}" );
			}

			ChangeStation( buttons );
		}

		private void ChangeStation( params bool[] buttonBools ) {
			for ( var i = 0; i < buttonBools.Length; ++i ) {
				if ( buttonBools[i] ) {

					if ( radio.Station != i ) {
						radio.Station = i;
					}

					Close();

					return;
				}
			}
		}
	}
}