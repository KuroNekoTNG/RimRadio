﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

using RimRadio.Runtime.Stations;

using UnityEngine;

using Verse;

namespace RimRadio.Music {
	/// <summary>
	/// This class is a singleton that controls the radio's list.
	/// It holds a readonly list and once it's contents are set, they cannot be removed unless you restart the game.
	/// This is to prevent any mods from kicking out others or over writing their spots.
	/// </summary>
	public class RadioPlaylistManager {

		internal class AllSongStation : RadioStation {

			private static readonly string ALL_SONG_ENABLE_PATH = Path.Combine( Application.persistentDataPath, "RimRadio", "AllSongStation.bin" );

			public override int Count => enabled.Count;

			public int AllCount {
				get {
					var count = 0;

					for ( int i = 1; i < Singleton.Count; ++i ) {
						count += Singleton[i].Count;
					}

					return count;
				}
			}

			public override List<SongInfo> Songs {
				get {
					if ( Singleton.Count <= 1 || cached.Count == AllCount ) {
						return cached;
					}

					RebuildCache();

					return cached;
				}
			}

			public override SongInfo this[int index] => enabled[index];

			// Used to prevent constant allocation of a List<T> when there is simply no need to.
			// Which also helps when allocating 
			private List<SongInfo> cached;
			private List<SongInfo> enabled;
			private Dictionary<string, bool> songsEnabled;

			public AllSongStation() : base( RimRadioMod.RimRadioInstence.Content.RootDir, "All Song Station", RuntimeStation.RUNTIME_STATION_ICON ) {
				cached = new List<SongInfo>();
				enabled = new List<SongInfo>();

				songsEnabled = new Dictionary<string, bool>();

				LoadBooleanArray();
			}

			internal bool IsSongEnabled( int index ) {
				SongInfo songInfo;

				try {
					songInfo = Songs[index];
				} catch ( ArgumentOutOfRangeException e ) {
					Logging.DebugError( string.Format( "Exception:\t{0}\nMessage:\t{1}\nValue:\t{2}\nStacktrace:\n{3}",
						e.GetType().FullName,
						e.Message,
						index,
						e.StackTrace
					) );

					//System.Threading.Tasks.Task.Delay( -1 ).Wait();

					RebuildCache( true );

					songInfo = cached[index];
				} catch ( Exception e ) {
					Logging.DebugError( e );

					throw;
				}

				if ( !songsEnabled.ContainsKey( songInfo.ToString() ) ) {
					songsEnabled.Add( songInfo.ToString(), true );
				}

				return songsEnabled[songInfo.ToString()];
			}

			internal void DisableSong( int index ) {
				var songInfo = cached[index];

				songsEnabled[songInfo.ToString()] = false;

				RebuildEnabled();

				SaveData();
			}

			internal void EnableSong( int index ) {
				var songInfo = cached[index];

				songsEnabled[songInfo.ToString()] = true;

				RebuildEnabled();

				SaveData();
			}

			private void LoadBooleanArray() {
				if ( !File.Exists( ALL_SONG_ENABLE_PATH ) || new FileInfo( ALL_SONG_ENABLE_PATH ).Length <= 3 ) {
					return;
				}

				using ( var binFileStream = new FileStream( ALL_SONG_ENABLE_PATH, FileMode.Open, FileAccess.Read, FileShare.Read ) ) {
					using ( var binReader = new BinaryReader( binFileStream, Encoding.Unicode ) ) {
						var count = binReader.ReadInt32();

						for ( int i = 0; i < count; ++i ) {
							var key = binReader.ReadString();
							var enabled = binReader.ReadBoolean();

							if ( !songsEnabled.ContainsKey( key ) ) {
								songsEnabled.Add( key, enabled );
							} else {
								songsEnabled[key] = enabled;
							}
						}
					}
				}
			}

			/* The structure and saving of the boolean array goes as follows:
			 * { Count of Entries } { { String Length } { String } (Encoded in UTF-16 Little Endian) } { boolean }
			 * All previous data will be overwritten upon saving data. This means that any previously saved files that no longer are in will be deleted. */
			private void SaveData() {
				if ( !new FileInfo( ALL_SONG_ENABLE_PATH ).Directory.Exists ) {
					new FileInfo( ALL_SONG_ENABLE_PATH ).Directory.Create();
				}

				using ( var binFileStream = new FileStream( ALL_SONG_ENABLE_PATH, FileMode.Create, FileAccess.Write, FileShare.Read ) ) {
					using ( var binWriter = new BinaryWriter( binFileStream, Encoding.Unicode ) ) {
						lock ( songsEnabled ) {
							binWriter.Write( songsEnabled.Count );

							foreach ( var pair in songsEnabled ) {
								Logging.DebugMessage( $"Saving `{pair.Key}` to `{ALL_SONG_ENABLE_PATH}`." );

								binWriter.Write( pair.Key );
								binWriter.Write( pair.Value );
							}
						}
					}
				}
			}

			private void RebuildCache( bool clearCache = false ) {
				if ( clearCache ) {
					cached.Clear();
				}

				enabled.Clear();

				for ( int i = 1; i < Singleton.Count; ++i ) {
					var station = Singleton[i];

					Logging.Message( $"Verifying songs from `{station.RadioName}`." );

					for ( int j = 0; j < station.Count; ++j ) {
						if ( cached.Contains( station[j] ) ) {
							continue;
						}

						cached.Add( station[j] );

						if ( !songsEnabled.ContainsKey( station[j].ToString() ) ) {
							songsEnabled.Add( station[j].ToString(), true );
						}
					}
				}

				RebuildEnabled();
			}

			private void RebuildEnabled() {
				enabled.Clear();

				for ( int i = 0; i < cached.Count; ++i ) {
					var song = cached[i];

					if ( songsEnabled[song.ToString()] ) {
						enabled.Add( song );
					}
				}
			}

		}

		#region Static

		/// <summary>
		/// Checks if the manager is running and if not to create a new one and return it.
		/// </summary>
		public static RadioPlaylistManager Singleton {
			get;
		} = new RadioPlaylistManager();

		#endregion

		/// <summary>
		/// Returns the amount of radio stations available.
		/// </summary>
		public int Count => stations.Count;

		/// <summary>
		/// Returns the radio station at that index.
		/// </summary>
		/// <param name="index">A value that is in the range of 0 to int.MaxValue.</param>
		/// <returns></returns>
		public RadioStation this[int index] => stations[index]; // Links it to radios[] get method.

		/// <summary>
		/// Holds the private list of radio stations.
		/// </summary>
		private readonly List<RadioStation> stations;

		/// <summary>
		/// A private constructor to prevent multiple versions of the manager.
		/// </summary>
		private RadioPlaylistManager() {
			// First station is going to get the AllSongStation.
			stations = new List<RadioStation> {
				new AllSongStation()
			};

			Log.Message( "RimRadio.Music.RadioPlaylistManager is now awake." ); // Logs that the manager is active in a dramatic way.
		}

		#region Public Methods

		/// <summary>
		/// Returns the radio station at that location.
		/// </summary>
		/// <param name="index">A non-negitive value that holds the location.</param>
		/// <returns><see cref="RadioStation"/> for what's contained.</returns>
		public RadioStation GetRadioChannel( int index ) => this[index]; // It just calls the get method.

		/// <summary>
		/// <see cref="object"/> for purpose of method.
		/// </summary>
		/// <returns></returns>
		public override string ToString() {
			var builder = new StringBuilder( "RadioPlaylistManager {" ); // Creates a new StringBuilder.

			foreach ( var station in stations ) { // Loops through all the stations.
				builder.Append( FormatToString( station ) ); // Appends the station information to the builder.
			}

			return builder.Append( "\n}" ).ToString(); // Appends a new line and returns the string.
		}

		#endregion

		internal void AddStationToPlaylist( RadioStation station ) {
			if ( station is null || station.Songs.Count == 0 ) {
				throw new ArgumentNullException( nameof( station ), "Station is either null or does not contain any songs." );
			}

			stations.Add( station );

			Logging.Message( $"Number of stations: {stations.Count}\nNumber of songs: {station.Songs.Count}" );
		}

		#region Private Methods

		/// <summary>
		/// This method formates the ToString method of the <paramref name="station"/>.
		/// </summary>
		/// <param name="station">The station to format the string of.</param>
		/// <returns>The formated string.</returns>
		private string FormatToString( RadioStation station ) {
			var builder = new StringBuilder( $"\n\tStation {{" );
			var stationStrings = station.ToString().Split( '\n' );

			foreach ( var line in stationStrings ) {
				builder.Append( $"\n\t\t{line}" );
			}

			return builder.Append( "\n\t}" ).ToString();
		}

		#endregion

	}
}