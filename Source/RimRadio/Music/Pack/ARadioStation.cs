﻿using System.IO;

using RimRadio.Utility;

using Verse;

namespace RimRadio.Music.Pack {
	/// <summary>
	/// This abstract class is meant to be a communication layer between the mod and the radio station.
	/// Extending this class will allow anyone to make a music pack easier.
	/// </summary>
	public abstract class ARadioStation : Mod {

		/// <summary>
		/// The location of the sounds directory.
		/// </summary>
		public string SoundPath {
			get;
		}

		/// <summary>
		/// Holds the information for the RadioInfo when it is created.
		/// </summary>
		protected RadioInfo radioInfo;

		/// <summary>
		/// Returns a value that tells the caller rather radioInfo is null or not.
		/// </summary>
		protected bool IsRadioInfoNull {
			get => radioInfo is null;
		}

		/// <summary>
		/// Constructer used to set everything in motion for the mod.
		/// </summary>
		/// <param name="content"></param>
		public ARadioStation( ModContentPack content ) : base( content ) {
			SoundPath = Path.Combine( content.RootDir, "Sounds" ); // Set's the SoundPath readonly property to the generated path.

			RadioPlaylistManager.Singleton.Register( this ); // Registers the radio station.
		}

		/// <summary>
		/// Used by RadioPlaylistManager to retrive the RadioInfo from the RadioStation.
		/// </summary>
		/// <returns>The radio info deserialized from the music file.</returns>
		public virtual RadioInfo GetRadioStation() {
			string[] fileChances = new string[] { // Holds an array of potential music files.
				Path.Combine( SoundPath, "music.rrb" ), // Generates the path for the music binary.
				Path.Combine( SoundPath, "music.rrs" ), // Generates the path for the custom music file.
				Path.Combine( SoundPath, "music.rrx" ), // Generates the path for the XML music file.
				Path.Combine( SoundPath, "music.rrml" ), // Generates the path for the XML music file.
				Path.Combine( SoundPath, "music.xml" ), // Generates the path for the XML music file.
			};

			if ( IsRadioInfoNull ) { // Checks to see if radio info is null.
				if ( File.Exists( fileChances[0] ) ) { // Checks to see if there is a binary file.
					radioInfo = DeserializeHelper.FromBin( fileChances[0], Content.RootDir ); // Deserializes the binary.
				} else if ( File.Exists( fileChances[1] ) ) { // Checks if it is the custom songs format.
					radioInfo = DeserializeHelper.FromRRS( fileChances[1], Content.RootDir ); // Deserializes the custon song format.
				} else if ( File.Exists( fileChances[2] ) ) { // Checks to see if it is XML.
					radioInfo = DeserializeHelper.FromXML( fileChances[2], Content.RootDir ); // Deserializes the XML.
				} else if (File.Exists( fileChances[3] ) ) { // Checks to see if it is XML.
					radioInfo = DeserializeHelper.FromXML( fileChances[3], Content.RootDir ); // Deserializes the XML.
				} else if ( File.Exists( fileChances[4] ) ) { // Checks to see if it is XML.
					radioInfo = DeserializeHelper.FromXML( fileChances[4], Content.RootDir ); // Deserializes the XML.
				} else {
					throw new FileNotFoundException( "Unable to locate music.rrb/rrs/rrx/rrml in the Sounds folder." ); // Throws an exception that it could not find the appropiate file.
				}
			}

			return radioInfo;
		}
	}
}