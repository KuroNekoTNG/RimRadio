﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

using RimWorld;

using UnityEngine;

using Verse;

namespace RimRadio.Music {
	/// <summary>
	/// A class to hold the information of a song.
	/// </summary>
	public class SongInfo {
		/// <summary>
		/// The songs name. Example: Aamon
		/// </summary>
		public string Title {
			get;
		}

		/// <summary>
		/// The artist of the song. Example: Kuuro
		/// </summary>
		public string Artist {
			get;
		}

		/// <summary>
		/// The album the song debuted in. Example: Aamon - Single
		/// </summary>
		public string Album {
			get;
		}

		public TimeOfDay AllowedTimeOfDay {
			get;
		}

		public ReadOnlyCollection<Season> AllowedSeasons => allowedSeasons.AsReadOnly();

		public AudioClip Clip {
			get;
			protected set;
		}

		private List<Season> allowedSeasons;

		/// <summary>
		/// Set's everything to the default values.
		/// </summary>
		public SongInfo() {
			Title = string.Empty;
			Artist = string.Empty;
			Album = string.Empty;

			Clip = null;

			allowedSeasons = null;
		}

		/// <summary>
		/// Deep copies the song def passed to it, and defaults it's properties.
		/// <see cref="SongInfo()"/>
		/// </summary>
		/// <param name="songDef"></param>
		public SongInfo( SongDef songDef ) : this() {
			Clip = songDef.clip;
			AllowedTimeOfDay = songDef.allowedTimeOfDay;

			allowedSeasons = songDef.allowedSeasons;
		}

		/// <summary>
		/// Deep copies the song def passed to it and assings the values passed to it.
		/// <see cref="SongInfo(SongDef)"/>
		/// </summary>
		/// <param name="songDef"></param>
		/// <param name="title"></param>
		/// <param name="artist"></param>
		/// <param name="album"></param>
		public SongInfo( SongDef songDef, string title, string artist, string album ) : this( songDef ) {
			Title = title;
			Artist = artist;
			Album = album;
		}

		/// <summary>
		/// Creates a SongInfo based on the basic things it needs.
		/// </summary>
		/// <param name="allowedSeasons">The seasons the SongInfo is allowed to play during.</param>
		/// <param name="allowedTimeOfDay">The time of day the SongInfo is allowed to be played.</param>
		/// <param name="clip">The audio clip to play the song.</param>
		/// <param name="title">The title of the song.</param>
		/// <param name="artist">The person who created the song.</param>
		/// <param name="album">The collection it is part of.</param>
		public SongInfo( AudioClip clip, string title, string artist, string album, TimeOfDay allowedTimeOfDay, List<Season> allowedSeasons ) {
			Title = title;
			Artist = artist;
			Album = album;
			AllowedTimeOfDay = allowedTimeOfDay;
			Clip = clip;

			this.allowedSeasons = allowedSeasons;
		}

		public SongInfo( string title, string artist, string album, TimeOfDay allowedTimeOfDay, AudioClip clip, params Season[] allowedSeasons )
			: this( clip, title, artist, album, allowedTimeOfDay, new List<Season>( allowedSeasons ) ) {

		}

		public SongInfo( string title, string artist, string album, AudioClip clip ) {
			Title = title;
			Artist = artist;
			Album = album;
			Clip = clip;
		}

		/// <summary>
		/// Returns a string that goes "[song name] | [artist] | [album]".
		/// See also: <seealso cref="object.ToString()"/>
		/// </summary>
		/// <returns>[song name] | [artist] | [album]</returns>
		public override string ToString() => $"{Title} | {Artist} | {Album}";
	}
}