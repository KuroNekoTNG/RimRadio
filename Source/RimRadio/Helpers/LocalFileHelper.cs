﻿using System.IO;
using System.Threading;
using System.Threading.Tasks;

using UnityEngine;
using UnityEngine.Networking;

namespace RimRadio.Helpers {
	internal static class LocalFileHelper {

		internal static Texture2D GetTextureWithoutWebRequest( string path ) {
			var tex = new Texture2D( 1, 1 );
			var rawData = ( byte[] )null;

			using ( var fileStream = new FileStream( path, FileMode.Open, FileAccess.Read, FileShare.Read ) ) {
				rawData = new byte[fileStream.Length];

				fileStream.Read( rawData );
			}

			ImageConversion.LoadImage( tex, rawData );

			return tex;
		}

		internal static Texture2D GetTextureUsingWebRequest( string uriPath ) {
			using ( var request = UnityWebRequestTexture.GetTexture( uriPath ) ) {
				DownloadUsingWebRequest( request );

				return ( ( DownloadHandlerTexture )request.downloadHandler ).texture;
			}
		}

#pragma warning disable CS0618 // Type or member is obsolete
		internal static AudioClip GetAudioClip( string uriPath ) {
			// FIXED: UnityWebRequest appears to not work for streaming local audio files.
			var www = new WWW( uriPath );

			while ( !www.isDone ) {
				Task.Delay( 128 ).Wait();
			}

			return www.GetAudioClip( false, true );
		}
#pragma warning restore CS0618 // Type or member is obsolete

		// Since all the UnityWebRequest extends UnityWebRequest, this method enables a single call
		// to handle the downloading so errors from using the UnityWebRequest don't happen.
		// it is also blocking so if something needs to be loaded on the main thread it can.
		private static void DownloadUsingWebRequest( UnityWebRequest webRequest ) {
			using ( var tokenSource = new CancellationTokenSource() ) {
				webRequest.SendWebRequest().completed += ( _ ) => tokenSource.Cancel();

				Task.Delay( -1, tokenSource.Token );
			}
		}

	}
}
