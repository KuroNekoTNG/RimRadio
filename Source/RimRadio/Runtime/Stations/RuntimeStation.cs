﻿using System.IO;

using RimRadio.Helpers;
using RimRadio.Music;

using UnityEngine;

using Verse;

namespace RimRadio.Runtime.Stations {

	/// <summary>
	/// RuntimeStations are special stations that are loaded at runtime.
	/// By loaded at runtime, I mean it generates dynamic song files as soon as it starts up.
	/// It is different from music packs since they are pre-defined to their own area.
	/// </summary>
	[StaticConstructorOnStartup]
	internal sealed class RuntimeStation : RadioStation {

		/// <summary>
		/// The icon that all RuntimeStations will use.
		/// </summary>
		internal static readonly Texture2D RUNTIME_STATION_ICON;

		static RuntimeStation() {
			var mod = RimRadioMod.RimRadioInstence;
			var iconLoc = Path.Combine( mod.Content.RootDir, "Textures", "RuntimeStation Icon.png" );

			Log.Message( $"Test 1 2" );

			RUNTIME_STATION_ICON = LocalFileHelper.GetTextureWithoutWebRequest( iconLoc );
		}

		/// <summary>
		/// Creates a new RuntimeStation with the information it needs.
		/// Defaults the modification path to RimRadio's root folder.
		/// </summary>
		/// <param name="radioName">The name the station will take on.</param>
		/// <param name="songs">The songs that are in the stations.</param>
		internal RuntimeStation( string radioName, params SongInfo[] songs ) : base( RimRadioMod.RimRadioInstence.Content.RootDir, radioName, RUNTIME_STATION_ICON, songs ) {

		}
	}
}
