﻿using NLayer;

using RimRadio.Music;

using TagLib;

using UnityEngine;

namespace RimRadio.Runtime.Stations.SongInfos {

	/// <summary>
	/// Represents songs loaded/streamed via NLayer.
	/// </summary>
	internal sealed class NLayerSongInfo : SongInfo {

		public string MpegFile {
			get;
		}

		private MpegFile mpeg;

		/// <summary>
		/// Creates a new representation of the song using a tagged file, and the path to the audio file.
		/// </summary>
		/// <seealso cref="NLayerSongInfo(string, string, string, string)"/>
		/// <param name="file">The path to the audio file.</param>
		/// <param name="tags">The tags of said audio file.</param>
		internal NLayerSongInfo( string file, File tags ) : this( tags.Tag.Title ?? "Unknown Title", tags.Tag.FirstPerformer ?? "Unknown Artist", tags.Tag.Album ?? "Unknown Album", file ) {

		}

		/// <summary>
		/// Creates a new, full representation of the song.
		/// </summary>
		/// <param name="title">The title of the song.</param>
		/// <param name="artist">The creator of the song.</param>
		/// <param name="album">The album the song belongs to.</param>
		/// <param name="file">The path to the audio file.</param>
		internal NLayerSongInfo( string title, string artist, string album, string file ) : base( title, artist, album, null ) {
			MpegFile = file;

			RecreateMpegStream();

			Logging.DebugMessage( $"Is Title null:\t{title is null}\nIs Artist null:\t{artist is null}\nIs Album null:\t{album is null}\nIs MPEG File null:\t{mpeg is null}" );

			var sampleCount = ( int )( mpeg.Length / ( mpeg.Channels * sizeof( float ) ) );

			Clip = AudioClip.Create( title, sampleCount, mpeg.Channels, mpeg.SampleRate, true, PullPcmData, SetPcmPosition );
		}

		~NLayerSongInfo() => mpeg?.Dispose();

		/// <summary>
		/// Used by Unity to enable streaming of the audio file.
		/// </summary>
		/// <param name="data">Data that unity requests.</param>
		private void PullPcmData( float[] data ) {
			Logging.DebugMessage( $"Reading `{data.Length * 4}` amount of data.", true );

			mpeg.ReadSamples( data, 0, data.Length );
		}

		/// <summary>
		/// Used by Unity to set the position for the stream.
		/// </summary>
		/// <param name="position">The new position to set it to.</param>
		private void SetPcmPosition( int position ) {
			Logging.DebugMessage( $"Checking to see if needing to recreate stream." );

			if ( position == 0 ) {
				Logging.DebugMessage( "Recreating the stream." );

				RecreateMpegStream();
			}

			Logging.DebugMessage( $"Setting position to `{position}`." );

			mpeg.Position = position;
		}

		private void RecreateMpegStream() {
			mpeg?.Dispose();

			mpeg = new MpegFile( MpegFile );
		}
	}
}