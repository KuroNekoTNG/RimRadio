﻿using RimRadio.Music;

using TagLib;

using UnityEngine;

namespace RimRadio.Runtime.Stations.SongInfos {
	internal sealed class UnitySongInfo : SongInfo {

		internal UnitySongInfo( File taggedFile, AudioClip clip ) : this( taggedFile.Tag.Title, taggedFile.Tag.FirstPerformer, taggedFile.Tag.Album, clip ) {

		}

		internal UnitySongInfo( string title, string artist, string album, AudioClip clip ) : base( title, artist, album, clip ) {

		}

	}
}