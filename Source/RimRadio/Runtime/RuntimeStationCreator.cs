﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using RimRadio.Helpers;
using RimRadio.Music;
using RimRadio.Runtime.Stations;
using RimRadio.Runtime.Stations.SongInfos;
using Verse;
using File = TagLib.File;

namespace RimRadio.Runtime {
	// TODO: Add documentation for this class.
	internal static class RuntimeStationCreator {
		// TODO: Add ability to customize the layout/order of the runtime stations.
		internal static void LoadRuntimeStations() {
			Logging.DebugMessage( $"Retrieving Audio Files.", true );

			string[] audioFiles;

			try {
				audioFiles = GetAudioFiles();
			} catch ( Exception e ) {
				Logging.DebugError( $"Exception:\t{e.GetType().FullName}\nMessage:\t{e.Message}\nException Stacktrace:\n{e.StackTrace}" );

				return;
			}

			Logging.DebugMessage( "Sorting audio for entering into stations.", true );

			Dictionary<string, SongInfo[]> earlyStations;

			try {
				earlyStations = SortAndLoadFiles( audioFiles );
			} catch ( Exception e ) {
				Logging.Error( e );

				return;
			}

			Logging.Message( "Putting files into stations.", true );

			// ReSharper disable once ForeachCanBePartlyConvertedToQueryUsingAnotherGetEnumerator
			foreach ( var pair in earlyStations ) {
				var station = new RuntimeStation( pair.Key, pair.Value );

				RadioPlaylistManager.Singleton.AddStationToPlaylist( station );
			}
		}

		private static string[] GetAudioFiles( string lookPath = "" ) {
			var files = new List<string>();

			if ( string.IsNullOrEmpty( lookPath ) ) {
				var paths = RimRadioSettings.musicDirs.Split( ';' );

				foreach ( var path in paths ) {
					if ( string.IsNullOrEmpty( path ) ) {
						continue;
					}

					Logging.DebugMessage( $"Exploring `{path}`." );

					try {
						files.AddRange( GetAudioFiles( path ) );
					} catch ( Exception e ) {
						// Normally would, but in this case, String Interpolation makes it way harder to read.
						// ReSharper disable once UseStringInterpolation
						Logging.DebugError(
							string.Format(
								"Exception:\t{0}\nMessage:\t{1}\nPassed path:\t{2}\nException caused at:\t{3}\nStacktrace:\n{4}",
								e.GetType().FullName,
								e.Message,
								path,
								new StackTrace( e ).GetFrame( 0 ).GetFileLineNumber(),
								e.StackTrace
							)
						);
					}
				}
			} else {
				/* Okay, funny thing can happen here, but I don't know how that would be fixed other than using a foreach.
				 * So, if the user has Junction points that link to files instead of folders, which is somehow possible,
				 * this will result in an error 267. Which is Invalid Directory Name, the kicker is that the period is allowed,
				 * so at first this is confusing (it is confusing unless you use a foreach to figure it out in the first place),
				 * but then you got to realize that Files are, most definitely, marked differently than folders. So this error
				 * is actually not really descriptive at all. Thanks Microsoft for making such wonderful non-descriptive errors.
				 *
				 * 26/11/2020 - Kyu Vulpes - What happened here? I don't even know what my own comment is really about? But I'm also afraid to mess something up.
				 * Shouldn't it just work like it should? */
				var potentialMusic = new DirectoryInfo( lookPath ).GetFiles( "*", SearchOption.AllDirectories );

				CreateFileListAndLog( potentialMusic );

				foreach ( var file in potentialMusic ) {
					Logging.DebugMessage( $"Checking if `{file}` is a valid audio file.", true );

					if ( !IsValidAudioFile( file ) ) {
						continue;
					}

					Logging.DebugMessage( $"`{file}` is a valid audio file.", true );

					files.Add( file.FullName );
				}
			}

			return files.ToArray();
		}

		private static bool IsValidAudioFile( string file ) {
			var ext = file.Substring( file.LastIndexOf( '.' ) + 1 );

			Logging.DebugMessage( $"The extension is `{ext}`.", true );

			return ext.Equals( "ogg", StringComparison.OrdinalIgnoreCase ) || ext.Equals( "oga", StringComparison.OrdinalIgnoreCase )
																		   || ext.Equals( "wav", StringComparison.OrdinalIgnoreCase ) || ext.Equals( "mp3", StringComparison.OrdinalIgnoreCase );
		}

		private static bool IsValidAudioFile( FileSystemInfo file ) => IsValidAudioFile( file.Extension );

		[Conditional( "DEBUG" )]
		private static void CreateFileListAndLog( IEnumerable<FileInfo> files ) {
			var builder = new StringBuilder();

			foreach ( var file in files ) {
				builder.AppendLine( file.FullName );
			}

			Logging.DebugMessage( builder.ToString().Trim() );
		}

		private static Dictionary<string, SongInfo[]> SortAndLoadFiles( IReadOnlyCollection<string> audioFiles ) {
			var sort             = new Dictionary<string, List<SongInfo>>();
			var returnDictionary = new Dictionary<string, SongInfo[]>();

			Logging.DebugMessage( $"Looping through all `{audioFiles.Count}` audio files.", true );

			foreach ( var file in audioFiles ) {
				var ext = file.Substring( file.LastIndexOf( '.' ) + 1 );

				try {
					LoadFile( file, ext );
				} catch ( Exception e ) {
					Logging.Error( e );
				}
			}

			Logging.Message( "Pruning dictionary and lists." );

			foreach ( var pair in sort ) {
				var arr = pair.Value;

				Logging.Message( $"Verifying no null references for {pair.Key}." );

				for ( var i = 0; i < arr.Count; ++i ) {
					Logging.DebugMessage( $"Is SongInfo null:\t{arr[i] is null}\nIs Clip null:\t{arr[i]?.Clip is null}" );

					if ( arr[i] != null && arr[i].Clip != null && arr[i].Clip ) {
						continue;
					}
					
					Logging.Message( "Clip or SongInfo is null, removing." );

					arr.RemoveAt( i-- );
				}

				if ( arr.Count == 0 ) {
					Logging.Message( $"Array associated with ${pair.Key} is empty, removing." );

					continue;
				}

				returnDictionary.Add( pair.Key, arr.ToArray() );
			}

			return returnDictionary;

			void LoadFile( string file, string ext ) {
				using ( var taggedFile = File.Create( file ) ) {
					var key = taggedFile.Tag.FirstPerformer ?? "Unknown Artist";

					Logging.DebugMessage( $"Key:\t{key}" );

					if ( !sort.ContainsKey( key ) ) {
						sort.Add( key, new List<SongInfo>() );
					}

					if ( ext.Equals( "ogg", StringComparison.OrdinalIgnoreCase ) || ext.Equals( "oga", StringComparison.OrdinalIgnoreCase )
																				 || ext.Equals( "wav", StringComparison.OrdinalIgnoreCase ) ) {
						Logging.DebugMessage( $"Using `{typeof( LocalFileHelper )}` to read `{file}`.", true );

						try {
							sort[key].Add( GenerateFromWWW( taggedFile, file ) );
						} catch ( Exception e ) {
							Logging.DebugError( e );
						}
					} else if ( ext.Equals( "mp3", StringComparison.OrdinalIgnoreCase ) ) {
						Logging.DebugMessage( $"Using NLayer to read `{file}`.", true );

						sort[key].Add( new NLayerSongInfo( file, taggedFile ) );
					}
				}
			}
		}

		private static UnitySongInfo GenerateFromWWW( File file, string path ) {
			var uriPath = GenFilePaths.SafeURIForUnityWWWFromPath( path );
			var clip    = LocalFileHelper.GetAudioClip( uriPath );

			if ( clip is null ) {
				throw new NullReferenceException( "Clip came back null, reason is unknown." );
			}

			return new UnitySongInfo( file.Tag.Title, file.Tag.FirstPerformer ?? "Unknown Artist", file.Tag.Album, clip );
		}
	}
}