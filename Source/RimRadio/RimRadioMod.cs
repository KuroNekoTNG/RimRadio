﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

using RimRadio.Helpers;
using RimRadio.Music;
using RimRadio.Music.Info;

using UnityEngine;

using Verse;

namespace RimRadio {

	/// <summary>
	/// This is the class that represents this mod.
	/// </summary>
	public sealed class RimRadioMod : Mod { // This is the starting class of this mod.

		private const string LUDEON_MAIN_STATION = "Ludeon Studio"; // In case I need to change this later.
		private const string UNKOWN_STATION = "Unknown Station by {0}"; // For what the Unknown station is called.

		public static string TextureFolder {
			get;
			private set;
		}

		internal static RimRadioMod RimRadioInstence {
			get;
			private set;
		}

		private List<CrossRefStation> crossRefStations;

		/// <summary>
		/// This is the constructor.
		/// </summary>
		/// <param name="content"></param>
		public RimRadioMod( ModContentPack content ) : base( content ) {
			RimRadioInstence = this;

			crossRefStations = new List<CrossRefStation>(); // Creates a new list to hold the ref stations.

			TextureFolder = Path.Combine( Content.RootDir, "Textures" );

			GatherAllSoundXml();

			GetSettings<RimRadioSettings>();
		}

		/// <summary>
		/// Writes the settings.
		/// </summary>
		public override void WriteSettings() => GetSettings<RimRadioSettings>().Write();

		/// <summary>
		/// Tells RimWorld the category.
		/// </summary>
		/// <returns>The name of this class.</returns>
		public override string SettingsCategory() => "RimRadio: Remade";

		/// <summary>
		/// Creates the settings window.
		/// </summary>
		/// <param name="inRect">The inside of the settings window.</param>
		public override void DoSettingsWindowContents( Rect inRect ) => RimRadioSettings.DoSettingsWindow( inRect );

		internal void LoadModStations() {
			var stationDictionary = new Dictionary<string, RadioStation>(); // Creates a new dictionary to store stations.

			foreach ( var song in DefDatabase<SongDef>.AllDefs ) { // Loops through all of the song defs that have been loaded.

				if ( song.modContentPack.IsCoreMod ) { // Checks to see if it is part of the core mod.
					AddSongToLudeonStation( song, ref stationDictionary ); // Calls this method to add the song to the station.
				} else if ( IsInStationFile( song.clipPath, out var containingStation, out var crossRefSong ) ) { // Checks to see if the station is in any refs.
					AddSongToKnownStation( song, containingStation, crossRefSong, ref stationDictionary ); // Adds it as a known station.
				} else {
					AddSongToUnknown( song, ref stationDictionary ); // Adds the song as an unknown station.
				}
			}

			foreach ( var station in stationDictionary.Values ) { // Loops through all of the stations that was created.
				RadioPlaylistManager.Singleton.AddStationToPlaylist( station ); // Adds the station to the RadioPlaylistManager.
			}

			crossRefStations = null;
		}

		private void AddSongToUnknown( SongDef song, ref Dictionary<string, RadioStation> stationDictionary ) {
			var unkownStationName = string.Format( UNKOWN_STATION, song.modContentPack.Name ); // Formats the string with the name of the mod.

			if ( !stationDictionary.ContainsKey( unkownStationName ) ) { // Checks to see if it has not set up the Unkown Station.
				stationDictionary.Add( unkownStationName, new RadioStation( song.modContentPack.RootDir, unkownStationName ) ); // Set's up the unknown station.
			}

			// Creates a new song info with all of the objects it needs to know.
			stationDictionary[unkownStationName].Songs.Add( new SongInfo( song, song.clip.name, "Unknown Artist", "Unknown Album" ) );
		}

		private void AddSongToKnownStation( SongDef song, CrossRefStation containingStation, CrossRefSong crossRefSong, ref Dictionary<string, RadioStation> stationDictionary ) {
			if ( !stationDictionary.ContainsKey( containingStation.StationName ) ) {// Checks to see if it has not set up the Unkown Station.
				var iconPath = Path.Combine( Path.Combine( song.modContentPack.RootDir, "Textures" ), containingStation.IconPath );

				stationDictionary.Add(
					containingStation.StationName,      // Set's the name of the station.
					new RadioStation(                       // Creates a new Radio Station.
						song.modContentPack.RootDir,        // The root of the mod dir.
						containingStation.StationName,      // The name of the station.
						GetStationIcon( iconPath )          // The station's icon.
				) );
			}

			// Creates a new song info with all of the objects it needs to know.
			stationDictionary[containingStation.StationName].Songs.Add( new SongInfo( song, crossRefSong.SongTitle, crossRefSong.SongArtist, crossRefSong.SongAlbum ) );
		}

		private void AddSongToLudeonStation( SongDef song, ref Dictionary<string, RadioStation> stationDictionary ) {
			if ( song.clip.name.StartsWith( "Noodle" ) ) { // Checks to see if the clip starts with Noodle.
				return; // Exit's the method.
			}

			if ( !stationDictionary.ContainsKey( LUDEON_MAIN_STATION ) ) { // Checks to see if the station has not been created.
				var path = Path.Combine( TextureFolder, "RimWorld.png" ); // Creates a new path that points to the texture.

				stationDictionary.Add( LUDEON_MAIN_STATION, new RadioStation( song.modContentPack.RootDir, "RimWorld OST", GetStationIcon( path ) ) ); // Set's up the new station.
			}

			// Creates a new song info with all of the objects it needs to know.
			stationDictionary[LUDEON_MAIN_STATION].Songs.Add( new SongInfo( song, song.clip.name.Replace( '_', ' ' ), LUDEON_MAIN_STATION, "RimWorld OST" ) );
		}

		private Texture2D GetStationIcon( string iconPath ) => LocalFileHelper.GetTextureWithoutWebRequest( iconPath );

		private bool IsInStationFile( string clipPath, out CrossRefStation containingStation, out CrossRefSong crossRefSong ) {
			containingStation = null; // Defaults the outs to null.
			crossRefSong = null;

			foreach ( var station in crossRefStations ) { // Loops through all the ref stations.
				foreach ( var song in station.CrossRefSongs ) { // Loops through all the ref songs.
					if ( song.SongPath.Equals( clipPath, StringComparison.OrdinalIgnoreCase ) ) { // Checks to see if they are the same.
						containingStation = station; // Set's the station.
						crossRefSong = song; // Set's the song.

						return true; // Returns true.
					}
				}
			}

			return false;
		}

		private void GatherAllSoundXml() {
			var xmlSerializer = new XmlSerializer( typeof( CrossRefStation ) ); // Creates an XML Serializer.
			var contents = LoadedModManager.RunningModsListForReading; // Creates a list of contents.

			foreach ( var content in contents ) { // Loops through all the contents.
				FileInfo[] stationFiles; // Will reference a FileInfo array.

				try {
					stationFiles = new DirectoryInfo( Path.Combine( content.RootDir, "Sounds" ) ).GetFiles( "Station_*.xml", SearchOption.TopDirectoryOnly ); // Attempts to find all Station_*.xml files.
				} catch {
					continue; // Goes to the next content.
				}

				Log.Message( $"Found {stationFiles.Length} of station files." ); // Reports the amount of station files it found.

				foreach ( var stationFile in stationFiles ) { // Loops through all station files.
					CrossRefStation crossRefStation;

					Log.Message( $"Processing: {stationFile.FullName}" ); // Messages that it is processing the station file.

					try {
						using ( Stream xmlReader = stationFile.OpenRead() ) { // Opens the station file for reading.
							crossRefStation = ( CrossRefStation )xmlSerializer.Deserialize( xmlReader ); // Deserializes it and cast it.
						}
					} catch ( Exception e ) {
						Log.Error( $"Failed to deserialize {stationFile.Name}. Please make sure the XML layout is correct.\nException: {e.GetType().FullName}\nMessage: {e.Message}\nStacktrace: {e.StackTrace}", true ); // This is incase something happens.
						continue;
					}

					crossRefStations.Add( crossRefStation ); // Adds it to the list of stations.
				}
			}
		}
	}
}