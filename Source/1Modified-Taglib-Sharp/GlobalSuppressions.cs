﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage(
	"Naming",
	"CA1712:Do not prefix enum values with type name",
	Justification = "Keeping as close as possible to the original.",
	Scope = "type",
	Target = "~T:TagLib.Mpeg.Version"
)]

[assembly: SuppressMessage( "Design", "CA1063:Implement IDisposable Correctly", Justification = "Maybe later.", Scope = "type", Target = "~T:TagLib.File" )]
[assembly: SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>", Scope = "member", Target = "~M:TagLib.Audible.File.#ctor(TagLib.File.IFileAbstraction,TagLib.ReadStyle)" )]
[assembly: SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>", Scope = "member", Target = "~M:TagLib.Audible.Tag.#ctor(TagLib.Audible.File,System.Int64)" )]
[assembly: SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>", Scope = "member", Target = "~M:TagLib.Audible.Tag.Parse(TagLib.ByteVector)" )]
[assembly: SuppressMessage( "Performance", "CA1819:Properties should not return arrays", Justification = "<Pending>", Scope = "member", Target = "~P:TagLib.ByteVector.Data" )]
[assembly: SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>", Scope = "member", Target = "~M:TagLib.ByteVector.StringTypeToEncoding(TagLib.StringType,TagLib.ByteVector)~System.Text.Encoding" )]
[assembly: SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>", Scope = "member", Target = "~P:TagLib.ByteVector.IsSynchronized" )]
[assembly: SuppressMessage( "Performance", "CA1819:Properties should not return arrays", Justification = "<Pending>", Scope = "member", Target = "~P:TagLib.CombinedTag.Tags" )]
[assembly: SuppressMessage( "Design", "CA1034:Nested types should not be visible", Justification = "<Pending>", Scope = "type", Target = "~T:TagLib.File.IFileAbstraction" )]
[assembly: SuppressMessage( "Performance", "CA1815:Override equals and operator equals on value types", Justification = "<Pending>", Scope = "type", Target = "~T:TagLib.Flac.BlockHeader" )]
[assembly: SuppressMessage( "Design", "CA1034:Nested types should not be visible", Justification = "<Pending>", Scope = "type", Target = "~T:TagLib.File.LocalFileAbstraction" )]
[assembly: SuppressMessage( "Performance", "CA1815:Override equals and operator equals on value types", Justification = "<Pending>", Scope = "type", Target = "~T:TagLib.Flac.StreamHeader" )]
[assembly: SuppressMessage( "Performance", "CA1819:Properties should not return arrays", Justification = "<Pending>", Scope = "member", Target = "~P:TagLib.Genres.Audio" )]
[assembly: SuppressMessage( "Performance", "CA1819:Properties should not return arrays", Justification = "<Pending>", Scope = "member", Target = "~P:TagLib.Genres.Video" )]
[assembly: SuppressMessage( "Performance", "CA1815:Override equals and operator equals on value types", Justification = "<Pending>", Scope = "type", Target = "~T:TagLib.Id3v2.Footer" )]
[assembly: SuppressMessage( "Style", "IDE0059:Unnecessary assignment of a value", Justification = "<Pending>", Scope = "member", Target = "~M:TagLib.Id3v2.Frame.FieldData(TagLib.ByteVector,System.Int32,System.Byte)~TagLib.ByteVector" )]
[assembly: SuppressMessage( "Performance", "CA1815:Override equals and operator equals on value types", Justification = "<Pending>", Scope = "type", Target = "~T:TagLib.Id3v2.FrameHeader" )]
[assembly: SuppressMessage( "Performance", "CA1819:Properties should not return arrays", Justification = "<Pending>", Scope = "member", Target = "~P:TagLib.Id3v2.RelativeVolumeFrame.Channels" )]
[assembly: SuppressMessage( "Performance", "CA1819:Properties should not return arrays", Justification = "<Pending>", Scope = "member", Target = "~P:TagLib.Id3v2.SynchronisedLyricsFrame.Text" )]
[assembly: SuppressMessage( "Performance", "CA1815:Override equals and operator equals on value types", Justification = "<Pending>", Scope = "type", Target = "~T:TagLib.Id3v2.SynchedText" )]
[assembly: SuppressMessage( "Performance", "CA1819:Properties should not return arrays", Justification = "<Pending>", Scope = "member", Target = "~P:TagLib.Id3v2.TextInformationFrame.Text" )]
[assembly: SuppressMessage( "Style", "IDE0059:Unnecessary assignment of a value", Justification = "<Pending>", Scope = "member", Target = "~M:TagLib.File.RFind(TagLib.ByteVector,System.Int64,TagLib.ByteVector)~System.Int64" )]
[assembly: SuppressMessage( "Performance", "CA1819:Properties should not return arrays", Justification = "<Pending>", Scope = "member", Target = "~P:TagLib.Id3v2.UrlLinkFrame.Text" )]
[assembly: SuppressMessage( "Performance", "CA1815:Override equals and operator equals on value types", Justification = "<Pending>", Scope = "type", Target = "~T:TagLib.Id3v2.Header" )]
[assembly: SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>", Scope = "member", Target = "~M:TagLib.Id3v2.UserUrlLinkFrame.#ctor(System.String,TagLib.StringType)" )]
[assembly: SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>", Scope = "member", Target = "~M:TagLib.Id3v2.Tag.Render~TagLib.ByteVector" )]
[assembly: SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>", Scope = "member", Target = "~M:TagLib.Id3v2.Tag.Parse(TagLib.ByteVector,TagLib.File,System.Int64,TagLib.ReadStyle)" )]
[assembly: SuppressMessage( "Performance", "CA1815:Override equals and operator equals on value types", Justification = "<Pending>", Scope = "type", Target = "~T:TagLib.Mpeg.AudioHeader" )]
[assembly: SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>", Scope = "member", Target = "~M:TagLib.Mpeg.AudioHeader.Find(TagLib.Mpeg.AudioHeader@,TagLib.File,System.Int64,System.Int32)~System.Boolean" )]
[assembly: SuppressMessage( "Performance", "CA1815:Override equals and operator equals on value types", Justification = "<Pending>", Scope = "type", Target = "~T:TagLib.Mpeg.VBRIHeader" )]
[assembly: SuppressMessage( "Style", "IDE0059:Unnecessary assignment of a value", Justification = "<Pending>", Scope = "member", Target = "~M:TagLib.Mpeg.VBRIHeader.#ctor(TagLib.ByteVector)" )]
[assembly: SuppressMessage( "Performance", "CA1815:Override equals and operator equals on value types", Justification = "<Pending>", Scope = "type", Target = "~T:TagLib.Mpeg.VideoHeader" )]
[assembly: SuppressMessage( "Performance", "CA1815:Override equals and operator equals on value types", Justification = "<Pending>", Scope = "type", Target = "~T:TagLib.Mpeg.XingHeader" )]
[assembly: SuppressMessage( "Style", "IDE0059:Unnecessary assignment of a value", Justification = "<Pending>", Scope = "member", Target = "~M:TagLib.Mpeg.XingHeader.#ctor(TagLib.ByteVector)" )]
[assembly: SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>", Scope = "member", Target = "~M:TagLib.NonContainer.EndTag.ReadTag(System.Int64@,TagLib.ReadStyle)~TagLib.Tag" )]
[assembly: SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>", Scope = "member", Target = "~M:TagLib.NonContainer.EndTag.ReadTagInfo(System.Int64@)~TagLib.TagTypes" )]
[assembly: SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>", Scope = "member", Target = "~M:TagLib.NonContainer.StartTag.ReadTagInfo(System.Int64@)~TagLib.TagTypes" )]
[assembly: SuppressMessage( "Performance", "CA1819:Properties should not return arrays", Justification = "<Pending>", Scope = "member", Target = "~P:TagLib.Ogg.Page.Packets" )]
[assembly: SuppressMessage( "Performance", "CA1819:Properties should not return arrays", Justification = "<Pending>", Scope = "member", Target = "~P:TagLib.Ogg.PageHeader.PacketSizes" )]
[assembly: SuppressMessage( "Style", "IDE0059:Unnecessary assignment of a value", Justification = "<Pending>", Scope = "member", Target = "~M:TagLib.Ogg.Paginator.Paginate(System.Int32@)~TagLib.Ogg.Page[]" )]
[assembly: SuppressMessage( "Style", "IDE0059:Unnecessary assignment of a value", Justification = "<Pending>", Scope = "member", Target = "~M:TagLib.File.ReadBlock(System.Int32)~TagLib.ByteVector" )]
[assembly: SuppressMessage( "Usage", "CA2217:Do not mark enums with FlagsAttribute", Justification = "<Pending>", Scope = "type", Target = "~T:TagLib.TagTypes" )]
[assembly: SuppressMessage( "Performance", "CA1819:Properties should not return arrays", Justification = "<Pending>", Scope = "member", Target = "~P:TagLib.Tag.Performers" )]
[assembly: SuppressMessage( "Performance", "CA1819:Properties should not return arrays", Justification = "<Pending>", Scope = "member", Target = "~P:TagLib.Tag.Genres" )]
[assembly: SuppressMessage( "Performance", "CA1819:Properties should not return arrays", Justification = "<Pending>", Scope = "member", Target = "~P:TagLib.Tag.Pictures" )]
[assembly: SuppressMessage( "Performance", "CA1819:Properties should not return arrays", Justification = "<Pending>", Scope = "member", Target = "~P:TagLib.Tag.PerformersSort" )]
[assembly: SuppressMessage( "Performance", "CA1819:Properties should not return arrays", Justification = "<Pending>", Scope = "member", Target = "~P:TagLib.Tag.PerformersRole" )]
[assembly: SuppressMessage( "Performance", "CA1819:Properties should not return arrays", Justification = "<Pending>", Scope = "member", Target = "~P:TagLib.Tag.AlbumArtists" )]
[assembly: SuppressMessage( "Performance", "CA1819:Properties should not return arrays", Justification = "<Pending>", Scope = "member", Target = "~P:TagLib.Tag.AlbumArtistsSort" )]
[assembly: SuppressMessage( "Performance", "CA1819:Properties should not return arrays", Justification = "<Pending>", Scope = "member", Target = "~P:TagLib.Tag.Composers" )]
[assembly: SuppressMessage( "Performance", "CA1819:Properties should not return arrays", Justification = "<Pending>", Scope = "member", Target = "~P:TagLib.Tag.ComposersSort" )]
[assembly: SuppressMessage( "Performance", "CA1819:Properties should not return arrays", Justification = "<Pending>", Scope = "member", Target = "~P:TagLib.Tag.Artists" )]
